import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/util/exports.dart';
import '../../bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import '../controllers/voucherhistory_controller.dart';
import 'bulk_print.dart';
import 'single_print.dart';

class VoucherhistoryView extends GetView<VoucherhistoryController> {
  var bController = Get.put(BluetoothconnectionController());

  @override
  Widget build(BuildContext context) {
    final VoucherhistoryController _tabx = Get.put(VoucherhistoryController());

    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              builBottomModalSheet(context);
            },
            icon: Icon(
              Icons.bluetooth,
              color: AppColors.whiteColor,
              size: Dimens.fontSize24,
            ),
          )
        ],
        title: Align(
            alignment: Alignment.centerLeft, child: Text('Voucher History')),
        backgroundColor: AppColors.primaryColor,
        bottom: TabBar(
          controller: _tabx.Tabcontroller,
          automaticIndicatorColorAdjustment: true,
          enableFeedback: true,
          unselectedLabelStyle: TextStyle(color: Colors.white60),
          tabs: _tabx.myTabs,
          labelStyle: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
          labelColor: AppColors.secondaryColor,
          indicatorColor: AppColors.secondaryColor,
          unselectedLabelColor: AppColors.whiteColor,
          overlayColor: MaterialStateProperty.all(AppColors.greenColor),
        ),
      ),
      body: Center(
        child: TabBarView(
          physics: BouncingScrollPhysics(),
          controller: _tabx.Tabcontroller,
          children: [SinglePrint(), BulkPrint()],
        ),
      ),
    );
  }

  Future<void> builBottomModalSheet(BuildContext context) {
    return showModalBottomSheet<void>(
        context: context,
        isDismissible: true,
        builder: (BuildContext context) {
          return Container(
              height: 250,
              color: AppColors.whiteColor,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: ListView(
                  padding: EdgeInsets.all(10),
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Device:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Obx(() => bController.devices.isEmpty
                            ? Center(child: Text('no Device'.tr))
                            : DropdownButton<BluetoothDevice>(
                                focusColor: Colors.white,
                                value: bController.device.value.name == ''
                                    ? bController.devices[0]
                                    : bController.device.value,
                                style: TextStyle(color: Colors.white),
                                iconEnabledColor: Colors.black,
                                items: bController.devices
                                    .map<DropdownMenuItem<BluetoothDevice>>(
                                        (product) {
                                  return DropdownMenuItem<BluetoothDevice>(
                                    value: product,
                                    child: Text(
                                      product.name!,
                                      style: TextStyle(color: Colors.orange),
                                    ),
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  bController.device.value = value!;
                                },
                              )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: AppColors.primaryColor),
                            onPressed: () {
                              print('refresh');
                              bController.initPlatformState();
                            },
                            child: Text(
                              'Refresh',
                              style: TextStyle(color: AppColors.whiteColor),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Obx(() => Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: bController.connected.value
                                        ? Colors.red
                                        : Colors.green),
                                onPressed: bController.connected.value
                                    ? bController.disconnect
                                    : bController.connect,
                                child: bController.connecting.value
                                    ? Center(
                                        child: CircularProgressIndicator(
                                          color: AppColors.whiteColor,
                                        ),
                                      )
                                    : Text(
                                        bController.connected.value
                                            ? 'Disconnect'
                                            : 'Connect',
                                        style: TextStyle(color: Colors.white),
                                      ),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ));
        });
  }
}
