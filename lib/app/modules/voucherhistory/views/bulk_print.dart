import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../common/util/exports.dart';
import '../../../routes/app_pages.dart';
import '../../printpage/printed_vouchers_model.dart';
import '../controllers/voucherhistory_controller.dart';

class BulkPrint extends GetView<VoucherhistoryController> {
  final VoucherhistoryController controller = Get.find();
  contentBox(context, id) {
    return Stack(
      children: <Widget>[
        Container(
          padding:
              EdgeInsets.only(left: 20, top: 45 + 20, right: 20, bottom: 20),
          margin: EdgeInsets.only(top: 45),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Confirm print',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 15,
              ),
              Flexible(
                child: Text(
                  'Are you sure to reprint birr?',
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 22,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: TextButton(
                        onPressed: () {
                          Logger().d('reprint Start......');
                          controller.bulkReprint(id);
                          Get.back();
                        },
                        child: Text(
                          'Confirm'.tr,
                          style: TextStyle(
                              fontSize: Dimens.fontSize18,
                              color: AppColors.primaryColor),
                        )),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: TextButton(
                        onPressed: () {
                          Get.back();
                        },
                        child: Text(
                          'Cancle'.tr,
                          style: TextStyle(
                              fontSize: Dimens.fontSize18,
                              color: AppColors.primaryColor),
                        )),
                  ),
                ],
              ),
            ],
          ),
        ),
        Positioned(
          left: 20,
          right: 20,
          child: CircleAvatar(
            backgroundColor: AppColors.primaryColor,
            radius: 45,
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(45)),
                child: Text(
                  'Print',
                  style: TextStyle(
                      fontSize: Dimens.fontSize28,
                      color: AppColors.secondaryColor),
                )),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => controller.isloading.value
        ? Center(child: CircularProgressIndicator())
        : controller.isloading.value == false && controller.pv.value.total == 0
            ? Center(
                child: Text('No data Avaliable'),
              )
            : ListView.builder(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                itemCount: controller.pv.value.data.length,
                itemBuilder: (context, index) {
                  PrintedVoucher data = controller.pv.value.data[index];
                  return GestureDetector(
                    onTap: () =>
                        Get.toNamed(Routes.BULKDETAIL, arguments: data.id),
                    // showDialog(
                    //     context: context,
                    //     builder: (context) => Dialog(
                    //           shape: RoundedRectangleBorder(
                    //             borderRadius: BorderRadius.circular(20),
                    //           ),
                    //           elevation: 0,
                    //           backgroundColor: Colors.transparent,
                    //           child: contentBox(context, data.id),
                    //         )),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: MediaQuery.of(context).size.height * 0.2,
                      color: Colors.green.shade50,
                      child: Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  color: AppColors.secondaryColor,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10))),
                              width: 70,
                              height: MediaQuery.of(context).size.height * 0.28,
                              child: Center(
                                child: Text(
                                  data.faceValue.toString() + '\nBirr',
                                  style: TextStyle(
                                      fontSize: Dimens.fontSize16,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.28,
                                decoration: BoxDecoration(
                                    color: AppColors.whiteColor,
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(10),
                                        bottomRight: Radius.circular(10))),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Expanded(child: Text('')),
                                      Expanded(
                                        child: Text(
                                          'Number of Cards: ${data.amount}',
                                          style: TextStyle(
                                              fontSize: Dimens.fontSize13),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          'Expire Date: ${Utils.convertToDate(Utils.toDate(data.regDate).toString())}',
                                          style: TextStyle(
                                              fontSize: Dimens.fontSize13),
                                        ),
                                      ),
                                      Expanded(child: Text('')),
                                      Expanded(
                                        child: Align(
                                          alignment: Alignment.topRight,
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.redAccent,
                                                borderRadius: BorderRadius.only(
                                                    bottomRight:
                                                        Radius.circular(10))),
                                            height: 20,
                                            width: 40,
                                            child: Center(
                                              child: Text(
                                                'sold',
                                                style: TextStyle(
                                                    color: AppColors.white,
                                                    fontStyle:
                                                        FontStyle.italic),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }));
  }
}
