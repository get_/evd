// To parse this JSON data, do
//
//     final bulkReprint = bulkReprintFromJson(jsonString);

import 'dart:convert';

List<BulkReprint> bulkReprintFromJson(String str) => List<BulkReprint>.from(
    json.decode(str).map((x) => BulkReprint.fromJson(x)));

String bulkReprintToJson(List<BulkReprint> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BulkReprint {
  BulkReprint({
    required this.id,
    required this.printId,
    required this.voucherId,
    required this.faceValue,
    required this.regDate,
    this.voucherNumber,
    required this.serialNo,
    required this.batchNo,
    required this.reprint_count,
  });

  String id;
  String printId;
  String voucherId;
  String faceValue;
  int regDate;
  dynamic voucherNumber;
  String serialNo;
  String batchNo;
  int reprint_count;

  factory BulkReprint.fromJson(Map<String, dynamic> json) => BulkReprint(
        id: json["id"],
        printId: json["print_id"],
        voucherId: json["voucher_id"],
        faceValue: json["face_value"],
        regDate: json["reg_date"],
        voucherNumber: json["voucher_number"],
        serialNo: json["serial_no"],
        batchNo: json["batch_no"],
        reprint_count: json["reprint_count"]
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "print_id": printId,
        "voucher_id": voucherId,
        "face_value": faceValue,
        "reg_date": regDate,
        "voucher_number": voucherNumber,
        "serial_no": serialNo,
        "batch_no": batchNo,
        "reprint_count":reprint_count,
      };
}
