import 'package:get/get.dart';

import '../controllers/voucherhistory_controller.dart';

class VoucherhistoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VoucherhistoryController>(
      () => VoucherhistoryController(),
    );
  }
}
