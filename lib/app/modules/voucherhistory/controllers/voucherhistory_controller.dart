// ignore_for_file: non_constant_identifier_names

import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:evd_mobile/app/data/api_helper.dart';
import 'package:evd_mobile/app/modules/bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import 'package:evd_mobile/app/modules/distributor/controllers/accountholder_list_controller.dart';
import 'package:evd_mobile/app/modules/printpage/api_error_model.dart';
import 'package:evd_mobile/app/modules/printpage/printed_vouchers_model.dart';
import 'package:evd_mobile/app/modules/voucherhistory/bulk_reprint_model.dart';
import 'package:evd_mobile/app/modules/voucherhistory/reprint_model.dart';
import 'package:evd_mobile/app/modules/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class VoucherhistoryController extends GetxController
    with SingleGetTickerProviderMixin, StateMixin<List<BulkReprint>> {
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'Single Print '),
    Tab(text: 'Bulk Print'),
  ];
  Rx<PrintedVouchers> pv = PrintedVouchers(data: [], total: 0).obs;
  final ApiHelper _apiHelper = Get.find();
  final BluetoothconnectionController blue =
      Get.put(BluetoothconnectionController());
  TabController? Tabcontroller;
  RxBool isloading = true.obs;
  List<PrintModel> list_voucher = [];

  List<BulkReprint> list_bulk_voucher = [];
  RxBool print_loading = false.obs;

  BluetoothconnectionController bcontroller =
      Get.find<BluetoothconnectionController>();
  @override
  void onInit() {
    super.onInit();
    Tabcontroller = TabController(vsync: this, length: myTabs.length);
  }

  @override
  void onReady() {
    getPrintedVouchers();
    super.onReady();
  }

  @override
  void onClose() {
    Tabcontroller!.dispose();
    super.onClose();
  }

  Future<void> refreshbatch(BuildContext context) async {
    return _apiHelper.getPrintedVouchers(0).futureValue((dynamic value) {
      change(bulkReprintFromJson(value), status: RxStatus.success());
      Logger().d(bulkReprintFromJson(value).length);
      final snackBar = SnackBar(
        padding: EdgeInsets.all(5),
        behavior: SnackBarBehavior.floating,
        content: Row(
          children: [
            Icon(Icons.info),
            Text(
              'Page Refreshed Successfully',
              style: TextStyle(color: AppColors.whiteColor),
            ),
          ],
        ),
        backgroundColor: (AppColors.secondaryColor),
        action: SnackBarAction(
          label: 'OK',
          textColor: AppColors.whiteColor,
          onPressed: () {},
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  getPrintedVouchers() async {
    _apiHelper.getPrintedVouchers(0).futureValue((value) {
      if (value.statusCode == 200) {
        isloading.value = false;
        pv.value = printedVouchersFromJson(value.body);
      } else if (value.statusCode == 500) {
        toast('operation faild');
      }
      Logger().d(value.statusCode);
    }, retryFunction: getPrintedVouchers);
  }

  AccountholderController account = Get.put(AccountholderController());

  bulkReprint(id) {
    try {
      _apiHelper.bulkReprint(id).futureValue((value) async {
        bool? isOn = await blue.bluetooth.isOn;
        bool? isConnected = await blue.bluetooth.isConnected;

        if (!isOn!) {
          blue.show('Turn on Bluetooth First');
        } else if (!isConnected!) {
          blue.show('No Bluetooth Connection');
        } else {
          print_loading.value = true;
          if (value.statusCode == 200) {
            list_bulk_voucher = bulkReprintFromJson(value.body);
            bcontroller.startRePrint(
                list_bulk_voucher, account.ds.agentUsername);
            print_loading.value = false;
          } else if (value.statusCode == 400) {
            var err = apiErrorFromJson(value.body);
            toast(err.msgs[0]);
          } else if (value.statusCode == 404) {
            Logger().d(value.statusCode);
            toast('Resource not found.');
          } else {
            Logger().d(value.statusCode);
            toast('operation failed');
          }
        }
      });
    } catch (e) {
      Logger().d(e);
    }
  }
}
