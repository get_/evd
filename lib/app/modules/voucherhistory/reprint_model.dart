// To parse this JSON data, do
//
//     final PrintModel = SinglePrintFromJson(jsonString);

import 'dart:convert';

List<PrintModel> SinglePrintFromJson(String str) =>
    List<PrintModel>.from(json.decode(str).map((x) => PrintModel.fromJson(x)));

String SinglePrintToJson(List<PrintModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PrintModel {
  PrintModel({
    this.id,
    this.batchId,
    this.batchNo,
    this.serialNo,
    this.faceValue,
    this.isPrint,
    this.reprintCount,
    this.operatorId,
    this.regDate,
    this.voucherNumber,
  });

  String? id;
  String? batchId;
  String? batchNo;
  String? serialNo;
  int? faceValue;
  bool? isPrint;
  int? reprintCount;
  String? operatorId;
  int? regDate;
  dynamic? voucherNumber;

  factory PrintModel.fromJson(Map<String, dynamic> json) => PrintModel(
        id: json["id"],
        batchId: json["batch_id"],
        batchNo: json["batch_no"],
        serialNo: json["serial_no"],
        faceValue: json["face_value"],
        isPrint: json["is_print"],
        reprintCount: json["reprint_count"],
        operatorId: json["operator_id"],
        regDate: json["reg_date"],
        voucherNumber: json["voucher_number"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "batch_id": batchId,
        "batch_no": batchNo,
        "serial_no": serialNo,
        "face_value": faceValue,
        "is_print": isPrint,
        "reprint_count": reprintCount,
        "operator_id": operatorId,
        "reg_date": regDate,
        "voucher_number": voucherNumber,
      };
}
