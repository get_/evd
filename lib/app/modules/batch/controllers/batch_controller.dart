import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:evd_mobile/app/data/api_helper.dart';
import 'package:evd_mobile/app/modules/distributor/controllers/accountholder_list_controller.dart';
import 'package:evd_mobile/app/modules/batch/account_balance_model.dart';
import 'package:evd_mobile/app/modules/batch/batch_model.dart';
import 'package:evd_mobile/app/modules/deposit/controllers/deposit_controller.dart';
import 'package:evd_mobile/app/modules/batch/vouvhers_count.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

// ignore: non_constant_identifier_names
class BatchController extends GetxController with StateMixin<VouchersCount> {
  //TODO: Implement BatchController

  final ApiHelper _apiHelper = Get.find();
  Batch dataList = Batch(data: [], total: 0);
  List<AccountBalance> balanceList = [];
  double currentBalance = 0.0;
  RxMap tolalWithFaceValue = {}.obs;

  @override
  void onReady() {
    super.onReady();
    if (Storage.storage.hasData('token')) {
      Get.put<BatchController>(BatchController());
      Get.put<AccountholderController>(AccountholderController());
      Get.put<DepositController>(DepositController());
      AccountholderController().getAccountHolder();
      DepositController().getDeposits();
      Logger().d('battchhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh.......');
      getFaceValueCount(false);
    } else {}
  }

  @override
  void onClose() {}
  getFaceValueCount(refresh) {
    change(null, status: RxStatus.empty());
    Logger().d('facevalue count...');
    _apiHelper.getActiveVoucherNumber().then((value) {
      if (value.statusCode == 200) {
        VouchersCount vCount = vouchersCountFromJson(value.body);
        // VouchersCount vCount = vouchersCountFromJson(value);
        Logger().d(vCount.fifteen);
        change(vCount, status: RxStatus.success());
      }
    });
  }

  getAccountBalance() {
    _apiHelper.getAccountBalance().futureValue((dynamic value) {
      balanceList = accountBalanceFromJson(value);
      balanceList.isNotEmpty
          ? currentBalance = balanceList[0].closingBalance
          : currentBalance;
      Logger().d(value);
    });
  }

  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  toggleDrawer() async {
    if (scaffoldKey.currentState!.isDrawerOpen) {
      scaffoldKey.currentState!.openEndDrawer();
    } else {
      scaffoldKey.currentState!.openDrawer();
    }
  }
}
