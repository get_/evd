// To parse this JSON data, do
//
//     final accountBalance = accountBalanceFromJson(jsonString);

import 'dart:convert';

List<AccountBalance> accountBalanceFromJson(String str) =>
    List<AccountBalance>.from(
        json.decode(str).map((x) => AccountBalance.fromJson(x)));

String accountBalanceToJson(List<AccountBalance> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AccountBalance {
  AccountBalance({
    required this.id,
    required this.accountId,
    required this.openingBalance,
    required this.closingBalance,
    required this.isClosed,
    required this.regDate,
    required this.updatedDate,
  });

  String id;
  String accountId;
  double openingBalance;
  double closingBalance;
  bool isClosed;
  int regDate;
  int updatedDate;

  factory AccountBalance.fromJson(Map<String, dynamic> json) => AccountBalance(
        id: json["id"],
        accountId: json["account_id"],
        openingBalance: json["opening_balance"],
        closingBalance: json["closing_balance"],
        isClosed: json["is_closed"],
        regDate: json["reg_date"],
        updatedDate: json["updated_date"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "account_id": accountId,
        "opening_balance": openingBalance,
        "closing_balance": closingBalance,
        "is_closed": isClosed,
        "reg_date": regDate,
        "updated_date": updatedDate,
      };
}
