
import 'package:evd_mobile/app/common/constants.dart';
import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:evd_mobile/app/modules/batch/controllers/batch_controller.dart';
import 'package:evd_mobile/app/modules/home/controllers/home_controller.dart';
import 'package:evd_mobile/app/modules/widgets/custom_drawer_widget.dart';
import 'package:evd_mobile/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';



class BatchView extends GetView<BatchController> {
  @override
  final controller = Get.put(BatchController());
  final homeController = Get.find<HomeController>();

  var index;

  @override
  Widget build(BuildContext context) {
    Future<void> _handleRefresh() async {
      return await Future.delayed(Duration(seconds: 2));
    }

    return Scaffold(
        appBar: AppBar(
          title: Align(
              alignment: Alignment.centerLeft, child: Text('DashBoard'.tr)),
          centerTitle: true,
          backgroundColor: AppColors.primaryColor,
          elevation: 0,
          actions: [
            IconButton(
                onPressed: () => controller.getFaceValueCount(true),
                icon: Icon(Icons.refresh))
          ],
        ),
        drawer: CustomDrawerWidget(controller: homeController),
        body: controller.obx((state) {
          return Container(
            margin: EdgeInsets.fromLTRB(15, 20, 15, 20),
            child: GridView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: Constants.FACEVALUE.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? 2
                        : 3,
                crossAxisSpacing: 18,
                mainAxisSpacing: 18,
                childAspectRatio: (1.5 / 1),
              ),
              itemBuilder: (
                context,
                index,
              ) {
                List<int> cards = [
                  state!.five,
                  state.ten,
                  state.fifteen,
                  state.twentyFive,
                  state.fifty,
                  state.hundred,
                  state.fiftyHundred,
                  state.thousand
                ];
                return InkWell(
                  onTap: () {
                    Get.toNamed(Routes.PRINTPAGE,
                        arguments: [Constants.FACEVALUE[index], cards[index]]);
                  },
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.all(
                            MediaQuery.of(context).size.width * 0.004),
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(1),
                                spreadRadius: 1,
                                blurRadius: 1,
                                offset:
                                    Offset(0, 0), // changes position of shadow
                              ),
                            ],
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            gradient: LinearGradient(
                                colors: [
                                  AppColors.primaryColor,
                                  Color(0xFF175D91)
                                ],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                tileMode: TileMode.clamp)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '${Constants.FACEVALUE[index]}',
                                  style: TextStyle(
                                      color: AppColors.secondaryColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: Dimens.fontSize26),
                                ),
                                Text(
                                  ' BIRR'.tr,
                                  style: TextStyle(
                                      color: Colors.orange.shade300,
                                      fontWeight: FontWeight.bold,
                                      fontSize: Dimens.fontSize22),
                                ),
                              ],
                            ),
                            Text(cards[index].toString() + ' Cards'.tr,
                                style: TextStyle(
                                    fontSize: Dimens.fontSize18,
                                    color: AppColors.whiteColor),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        }));
  }
}
