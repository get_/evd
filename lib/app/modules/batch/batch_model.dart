// To parse this JSON data, do
//
//     final batch = batchFromJson(jsonString);

import 'dart:convert';

Batch batchFromJson(String str) => Batch.fromJson(json.decode(str));

String batchToJson(Batch data) => json.encode(data.toJson());

class Batch {
  Batch({
    required this.data,
    required this.total,
  });

  List<BatchData> data;
  int total;

  factory Batch.fromJson(Map<String, dynamic> json) => Batch(
        data: List<BatchData>.from(
            json["data"].map((x) => BatchData.fromJson(x))),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class BatchData {
  BatchData({
    required this.id,
    required this.poNumber,
    required this.batchNo,
    required this.vouchertype,
    required this.quantity,
    required this.startingSerialNo,
    required this.faceValue,
    required this.expireDate,
    required this.regDate,
  });

  String id;
  String poNumber;
  String batchNo;
  int vouchertype;
  int quantity;
  String startingSerialNo;
  int faceValue;
  int expireDate;
  int regDate;

  factory BatchData.fromJson(Map<String, dynamic> json) => BatchData(
        id: json["id"],
        poNumber: json["po_number"],
        batchNo: json["batch_no"],
        vouchertype: json["vouchertype"],
        quantity: json["quantity"],
        startingSerialNo: json["starting_serial_no"],
        faceValue: json["face_value"],
        expireDate: json["expire_date"],
        regDate: json["reg_date"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "po_number": poNumber,
        "batch_no": batchNo,
        "vouchertype": vouchertype,
        "quantity": quantity,
        "starting_serial_no": startingSerialNo,
        "face_value": faceValue,
        "expire_date": expireDate,
        "reg_date": regDate,
      };
}
