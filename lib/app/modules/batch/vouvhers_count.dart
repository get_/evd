// To parse this JSON data, do
//
//     final vouchersCount = vouchersCountFromJson(jsonString);

import 'dart:convert';

VouchersCount vouchersCountFromJson(String str) =>
    VouchersCount.fromJson(json.decode(str));

String vouchersCountToJson(VouchersCount data) => json.encode(data.toJson());

class VouchersCount {
  VouchersCount({
    required this.five,
    required this.ten,
    required this.fifty,
    required this.twentyFive,
    required this.fifteen,
    required this.hundred,
    required this.fiftyHundred,
    required this.thousand,
  });

  int five;
  int ten;
  int fifty;
  int twentyFive;
  int fifteen;
  int hundred;
  int fiftyHundred;
  int thousand;

  factory VouchersCount.fromJson(Map<String, dynamic> json) => VouchersCount(
        five: json["five"],
        ten: json["ten"],
        fifty: json["fifty"],
        twentyFive: json["twenty_five"],
        fifteen: json["fifteen"],
        hundred: json["hundred"],
        fiftyHundred: json["fifty_hundred"],
        thousand: json["thousand"],
      );

  Map<String, dynamic> toJson() => {
        "five": five,
        "ten": ten,
        "fifty": fifty,
        "twenty five": twentyFive,
        "fifteen": fifteen,
        "hundred": hundred,
        "fifty hundred": fiftyHundred,
        "thousand": thousand,
      };
}
