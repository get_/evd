import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../data/api_helper.dart';
import '../../bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import '../../distributor/controllers/accountholder_list_controller.dart';
import '../../printpage/api_error_model.dart';
import '../../printpage/printed_vouchers_model.dart';
import '../../voucherhistory/bulk_reprint_model.dart';
import '../../voucherhistory/reprint_model.dart';
import '../../widgets/toast.dart';

class PrinthistoryController extends GetxController
    with StateMixin<PrintedVouchers> {
  //TODO: Implement PrinthistoryController

  Rx<PrintedVouchers> pv = PrintedVouchers(data: [], total: 0).obs;
  final ApiHelper _apiHelper = Get.find();
  final BluetoothconnectionController blue =
      Get.find<BluetoothconnectionController>();

  RxBool isloading = true.obs;
  List<PrintModel> list_voucher = [];

  List<BulkReprint> list_bulk_voucher = [];
  RxBool print_loading = false.obs;
  RxBool longpressed = false.obs;
  BluetoothconnectionController bcontroller =
      Get.find<BluetoothconnectionController>();

  int page = 0;
  int maxPage = 0;
  PrintedVouchers loaded = PrintedVouchers(data: [], total: 0);
  final ScrollController scrollController = ScrollController();

  @override
  void onInit() {
    getPrintedVouchers();
    scrollController.addListener(pagination);

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void pagination() {
    if (scrollController.position.atEdge &&
        state != null &&
        pv.value.total != pv.value.data.length &&
        !isloading.value) {
      getVouchers(false);
    }
  }

  Future<void> getVouchers(bool refresh) async {
    //change(null, status: RxStatus.empty());
    if (maxPage == 0) {
      _apiHelper.getPrintedVouchers(page).then((value) async {
        if (value.statusCode == 200) {
          PrintedVouchers bulkprints = printedVouchersFromJson(value.body);
          if (refresh) {
             pv.value.data
              .sort((date1, date2) => date2.regDate.compareTo(date1.regDate));
            pv.value.data.addAll(bulkprints.data);
            isloading.value = false;
          }
          if (bulkprints.data.isNotEmpty) {
            pv.value.data.addAll(bulkprints.data);
            isloading.value = false;
            page++;
          } else {
            maxPage = page;
          }
          pv.value.data
              .sort((date1, date2) => date2.regDate.compareTo(date1.regDate));

          change(pv.value, status: RxStatus.success());
        } else {
          toast("can not load data".tr);
          change(pv.value, status: RxStatus.success());
        }
      });
    } else {
      toast("no more data");
      change(pv.value, status: RxStatus.success());
    }
  }

  getPrintedVouchers() async {
    // change(null, status: RxStatus.empty());
    _apiHelper.getPrintedVouchers(0).then((value) {
      isloading.value = false;
      if (value.statusCode == 200) {
        pv.value = printedVouchersFromJson(value.body);
        pv.value.data
            .sort((date1, date2) => date2.regDate.compareTo(date1.regDate));

        change(pv.value, status: RxStatus.success());
      } else {
        ApiError err = apiErrorFromJson(value.body);
        change(null, status: RxStatus.error('Something went wrong.'));
        if (err.msgs[0] == '') {
          change(null, status: RxStatus.error('Something went wrong.'));
        } else {
          toast(err.msgs[0]);
        }
      }
    },
        onError: (err) =>
            change(null, status: RxStatus.error('Something went wrong.')));
    page++;
  }

  AccountholderController account = Get.put(AccountholderController());

  bulkReprint(id) async {
    try {
      bool? isOn = await blue.bluetooth.isOn;
      bool? isConnected = await blue.bluetooth.isConnected;

      if (!isOn!) {
        blue.show('Turn on Bluetooth First');
      } else if (!isConnected!) {
        blue.show('No Bluetooth Connection');
      } else {
        _apiHelper.bulkReprint(id).then((value) {
          print_loading.value = true;
          if (value.statusCode == 200) {
            list_bulk_voucher = bulkReprintFromJson(value.body);
            bcontroller.startRePrint(
                list_bulk_voucher, account.ds.agentUsername);
            print_loading.value = false;
          } else if (value.statusCode == 400) {
            var err = apiErrorFromJson(value.body);
            toast(err.msgs[0]);
          } else if (value.statusCode == 404) {
            Logger().d(value.statusCode);
            toast('Resource not found.');
          } else {
            Logger().d(value.statusCode);
            toast('operation failed');
          }
        });
      }
    } catch (e) {
      Logger().d(e);
    }
  }
}
