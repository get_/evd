import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../common/util/exports.dart';
import '../../../routes/app_pages.dart';
import '../../bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import '../../home/controllers/home_controller.dart';
import '../../printpage/printed_vouchers_model.dart';
import '../../widgets/custom_drawer_widget.dart';
import '../controllers/printhistory_controller.dart';

class PrinthistoryView extends GetView<PrinthistoryController> {
  final homeController = Get.find<HomeController>();
  var bController = Get.find<BluetoothconnectionController>();

  @override
  final PrinthistoryController controller = Get.find<PrinthistoryController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              builBottomModalSheet(context);
            },
            icon: Icon(
              Icons.bluetooth,
              color: AppColors.whiteColor,
              size: Dimens.fontSize24,
            ),
          )
        ],
        title: Align(
            alignment: Alignment.centerLeft,
            child: Text(Strings.printHistory.tr)),
        centerTitle: true,
        backgroundColor: AppColors.primaryColor,
        elevation: 0,
      ),
      drawer: CustomDrawerWidget(controller: homeController),
      body: controller.obx((state) => controller.isloading.value
          ? Center(child: CircularProgressIndicator())
          : controller.pv.value.total == 0
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image(image: AssetImage(AppImages.nodata)),
                      Text('There are no sold mobile cards'.tr),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'please try again'.tr,
                          style: TextStyle(fontSize: Dimens.fontSize18),
                        ),
                      ),
                    ],
                  ),
                )
              : Scrollbar(
                  child: ListView.builder(
                      physics: const AlwaysScrollableScrollPhysics(),
                      controller: controller.scrollController,
                      shrinkWrap: true,
                      itemCount: state!.data.length,
                      itemBuilder: (context, index) {
                        PrintedVoucher data = state.data[index];
                        return Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          height: MediaQuery.of(context).size.height * 0.25,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 18),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 3,
                                            blurRadius: 4,
                                            offset: Offset(0,
                                                0), // changes position of shadow
                                          )
                                        ],
                                        color: AppColors.whiteColor,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            color:
                                                Colors.orange.withOpacity(0.5),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                Text('     Cards'.tr),
                                                Text('       Quantity'.tr),
                                                Text('Total[Birr]'.tr),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                Text(
                                                  data.faceValue.toString() +
                                                      '.00 ' +
                                                      ' BIRR'.tr,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Text(
                                                    'x ${Utils.numberFormat(data.amount)}',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Text(
                                                    '${data.amount * data.faceValue} ',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold))
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          color: Colors.grey.shade200,
                                          height: 3,
                                          width:
                                              MediaQuery.of(context).size.width,
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 10),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              TextButton.icon(
                                                  style:
                                                      ElevatedButton.styleFrom(
                                                    textStyle: TextStyle(
                                                        color: AppColors
                                                            .primaryColor),
                                                  ),
                                                  onPressed: () {
                                                    Logger().d(
                                                        'reprint Start......');
                                                    controller
                                                        .bulkReprint(data.id);
                                                  },
                                                  icon: Icon(Icons.print),
                                                  label: Text('Reprint'
                                                      .tr
                                                      .toUpperCase())),
                                              TextButton.icon(
                                                style: ElevatedButton.styleFrom(
                                                  textStyle: TextStyle(
                                                      color: AppColors
                                                          .primaryColor),
                                                ),
                                                onPressed: () {
                                                  Get.toNamed(Routes.BULKDETAIL,
                                                      arguments: data.id);
                                                },
                                                label: Text(
                                                    'detail'.tr.toUpperCase()),
                                                icon: Icon(
                                                  Icons.shortcut,
                                                  size: 20,
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                )),
    );
  }

  // contentBox(context, id) {
  //   return Stack(
  //     children: <Widget>[
  //       Container(
  //         padding:
  //             EdgeInsets.only(left: 20, top: 45 + 20, right: 20, bottom: 20),
  //         margin: EdgeInsets.only(top: 45),
  //         decoration: BoxDecoration(
  //             shape: BoxShape.rectangle,
  //             color: Colors.white,
  //             borderRadius: BorderRadius.circular(20),
  //             boxShadow: [
  //               BoxShadow(
  //                   color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
  //             ]),
  //         child: Column(
  //           mainAxisSize: MainAxisSize.min,
  //           children: <Widget>[
  //             Text(
  //               'Confirm print'.tr,
  //               style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
  //             ),
  //             SizedBox(
  //               height: 15,
  //             ),
  //             Flexible(
  //               child: Text(
  //                 'Are you sure you want to print'.tr,
  //                 style: TextStyle(fontSize: 14),
  //                 textAlign: TextAlign.center,
  //               ),
  //             ),
  //             SizedBox(
  //               height: 22,
  //             ),
  //             Row(
  //               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //               children: [
  //                 Align(
  //                   alignment: Alignment.bottomLeft,
  //                   child: TextButton(
  //                       onPressed: () {
  //                         Logger().d('reprint Start......');
  //                         controller.bulkReprint(id);
  //                         Get.back();
  //                       },
  //                       child: Text(
  //                         'Confirm'.tr,
  //                         style: TextStyle(
  //                             fontSize: Dimens.fontSize18,
  //                             color: AppColors.primaryColor),
  //                       )),
  //                 ),
  //                 Align(
  //                   alignment: Alignment.bottomRight,
  //                   child: TextButton(
  //                       onPressed: () {
  //                         Get.back();
  //                       },
  //                       child: Text(
  //                         'Cancel'.tr,
  //                         style: TextStyle(
  //                             fontSize: Dimens.fontSize18,
  //                             color: AppColors.primaryColor),
  //                       )),
  //                 ),
  //               ],
  //             ),
  //           ],
  //         ),
  //       ),
  //       Positioned(
  //         left: 20,
  //         right: 20,
  //         child: CircleAvatar(
  //           backgroundColor: AppColors.secondaryColor,
  //           radius: 45,
  //           child: ClipRRect(
  //               borderRadius: BorderRadius.all(Radius.circular(45)),
  //               child: Icon(Icons.print,
  //                   color: AppColors.whiteColor,
  //                   size: MediaQuery.of(context).size.width * 0.2)),
  //         ),
  //       ),
  //     ],
  //   );
  // }

  Future<void> builBottomModalSheet(BuildContext context) {
    return showModalBottomSheet<void>(
        context: context,
        isDismissible: true,
        builder: (BuildContext context) {
          return Container(
              height: 250,
              color: AppColors.whiteColor,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: ListView(
                  padding: EdgeInsets.all(10),
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Device'.tr + ':',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Obx(() => bController.devices.isEmpty
                            ? Center(child: Text('No Device'.tr))
                            : DropdownButton<BluetoothDevice>(
                                hint: Text('select device'.tr),
                                focusColor: Colors.white,
                                value: bController.device.value.name == ''
                                    ? null
                                    : bController.device.value,
                                style: TextStyle(color: Colors.white),
                                iconEnabledColor: Colors.black,
                                items: bController.devices
                                    .map<DropdownMenuItem<BluetoothDevice>>(
                                        (product) {
                                  return DropdownMenuItem<BluetoothDevice>(
                                    value: product,
                                    child: Text(
                                      product.name!,
                                      style: TextStyle(color: Colors.orange),
                                    ),
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  bController.device.value = value!;
                                },
                              )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: AppColors.primaryColor),
                            onPressed: () {
                              bController.initPlatformState();
                            },
                            child: Text(
                              'Refresh'.tr,
                              style: TextStyle(color: AppColors.whiteColor),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Obx(() => Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: bController.connected.value
                                        ? Colors.red
                                        : Colors.green),
                                onPressed: bController.connected.value
                                    ? bController.disconnect
                                    : bController.connect,
                                child: bController.connecting.value
                                    ? Container(
                                        width: 100,
                                        height: 20,
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: CircularProgressIndicator(
                                                color: AppColors.whiteColor,
                                              ),
                                            ),
                                            Expanded(
                                              child: Text(
                                                'loading...',
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color:
                                                        AppColors.whiteColor),
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    : Text(
                                        bController.connected.value
                                            ? 'Disconnect'.tr
                                            : 'Connect'.tr,
                                        style: TextStyle(color: Colors.white),
                                      ),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ));
        });
  }
}
