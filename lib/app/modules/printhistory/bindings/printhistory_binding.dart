import 'package:get/get.dart';

import '../controllers/printhistory_controller.dart';

class PrinthistoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PrinthistoryController>(
      () => PrinthistoryController(),
    );
  }
}
