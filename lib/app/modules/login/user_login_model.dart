import 'dart:convert';

DistributerModel DistributerModelFromJson(String str) =>
    DistributerModel.fromJson(json.decode(str));

String DistributerModelToJson(DistributerModel data) =>
    json.encode(data.toJson());

class DistributerModel {
  DistributerModel({
    required this.un,
    required this.pwd,
  });

  String un;
  String pwd;

  factory DistributerModel.fromJson(Map<String, dynamic> json) =>
      DistributerModel(
        un: json["un"],
        pwd: json["pwd"],
      );

  Map<String, dynamic> toJson() => {
        "un": un,
        "pwd": pwd,
      };
}
