import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../common/util/exports.dart';
import '../../../common/util/validators.dart';
import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  final _formKey = GlobalKey<FormState>();
  final controller = Get.put(LoginController());

  // final HomeController hcontroller = Get.put(HomeController());

  var autovalidate = AutovalidateMode.disabled;
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
          height: height,
          child: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: AppColors.primaryColor,
              ),
              Positioned(
                top: MediaQuery.of(context).size.height * 0.09,
                left: MediaQuery.of(context).size.width * 0.3,
                child: Center(
                  child: Image(
                    height: MediaQuery.of(context).size.height * 0.17,
                    width: MediaQuery.of(context).size.height * 0.2,
                    image: AssetImage(
                      AppImages.logo,
                    ),
                  ),
                ),
              ),
              Positioned(
                top: MediaQuery.of(context).size.height * 0.25,
                right: MediaQuery.of(context).size.width * 0.03,
                child: Center(
                  child: Obx(
                    () => TextButton(
                      onPressed: () {
                        controller.isAmharic.value =
                            !controller.isAmharic.value;
                        if (controller.isAmharic.value == true) {
                          Storage.storage.write('lang', 'አማርኛ');
                          controller.langCtrl.changeLocale('አማርኛ');
                        } else if (controller.isAmharic.value == false) {
                          Storage.storage.write('lang', 'English');
                          controller.langCtrl.changeLocale('English');
                        }
                      },
                      child: Text(
                        controller.isAmharic.value ? 'አማ' : 'EN',
                        style: TextStyle(color: AppColors.secondaryColor),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                  top: MediaQuery.of(context).size.height * 0.3,
                  child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30)),
                      ),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.7,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 50),
                        child: Form(
                          key: _formKey,
                          autovalidateMode: autovalidate,
                          child: SingleChildScrollView(
                            reverse: true,
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Phone Number'.tr,
                                  style: TextStyle(
                                      fontSize: Dimens.fontSize16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                TextFormField(
                                    keyboardType: TextInputType.phone,
                                    enableInteractiveSelection: true,
                                    controller: controller.userPhoneController,
                                    validator: (value) {
                                      return Validators.phoneValidation(value);
                                    },
                                    decoration: InputDecoration(
                                      hintText: 'Enter phone number'.tr,
                                      hintStyle: TextStyle(
                                          fontSize: Dimens.fontSize12,
                                          color: Colors.grey.shade500),
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                            color: AppColors.primaryColor,
                                            width: 2.0),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                          color: AppColors.primaryColor,
                                          width: 2.0,
                                        ),
                                      ),
                                    )),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  'Password'.tr,
                                  style: TextStyle(
                                      fontSize: Dimens.fontSize16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.normal),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Obx(() => TextFormField(
                                    enableInteractiveSelection: true,
                                    keyboardType: TextInputType.text,
                                    controller:
                                        controller.userPasswordController,
                                    obscureText:
                                        controller.passwordVisible.value,
                                    validator: (value) {
                                      return Validators.validatePassword(value);
                                    },
                                    decoration: InputDecoration(
                                      hintText: 'Enter password'.tr,
                                      hintStyle: TextStyle(
                                          fontSize: Dimens.fontSize12,
                                          color: Colors.grey.shade500),
                                      suffixIcon: IconButton(
                                        icon: Icon(
                                          // Based on passwordVisible state choose the icon
                                          controller.passwordVisible.value
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          color:
                                              controller.passwordVisible.value
                                                  ? AppColors.primaryColor
                                                  : AppColors.secondaryColor,
                                        ),
                                        onPressed: () {
                                          // Update the state i.e. toogle the state of passwordVisible variable
                                          controller.passwordVisible.value =
                                              !controller.passwordVisible.value;
                                        },
                                      ),
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                            color: AppColors.primaryColor,
                                            width: 2.0),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        borderSide: BorderSide(
                                          color: AppColors.primaryColor,
                                          width: 2.0,
                                        ),
                                      ),
                                    ))),
                                SizedBox(
                                  height: 8,
                                ),
                                SizedBox(height: 20.0),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal:
                                          MediaQuery.of(context).size.width *
                                              0.1),
                                  child: MaterialButton(
                                    height: MediaQuery.of(context).size.height *
                                        0.07,
                                    minWidth: double.infinity / 2,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(29)),
                                    onPressed: () {
                                      if (_formKey.currentState!.validate()) {
                                        controller.loginhandler();
                                      } else {
                                        autovalidate =
                                            AutovalidateMode.onUserInteraction;
                                      }
                                    },
                                    child: Text(
                                      "Sign in".tr,
                                      style: TextStyle(
                                        fontSize: Dimens.fontSize20,
                                        color: AppColors.whiteColor,
                                      ),
                                    ),
                                    color: AppColors.primaryColor,
                                  ),
                                ),
                                Divider(),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    // Expanded(child: Text('')),
                                    // Icon(Icons.language),
                                    // Expanded(child: Text(' Language'.tr)),
                                    // Center(
                                    //   child: Obx(
                                    //     () => TextButton(
                                    //       onPressed: () {
                                    //         controller.isAmharic.value =
                                    //             !controller.isAmharic.value;
                                    //         if (controller.isAmharic.value ==
                                    //             true) {
                                    //           Storage.storage
                                    //               .write('lang', 'አማርኛ');
                                    //           controller.langCtrl
                                    //               .changeLocale('አማርኛ');
                                    //         } else if (controller
                                    //                 .isAmharic.value ==
                                    //             false) {
                                    //           Storage.storage
                                    //               .write('lang', 'English');
                                    //           controller.langCtrl
                                    //               .changeLocale('English');
                                    //         }
                                    //       },
                                    //       child: Text(controller.isAmharic.value
                                    //           ? 'አማ'
                                    //           : 'EN'),
                                    //     ),
                                    //   ),
                                    // ),
                                    // // Expanded(
                                    // //   child: Container(
                                    // //     width:
                                    // //         MediaQuery.of(context).size.width,
                                    // //     child: ListTile(
                                    // //       title: DropdownButtonHideUnderline(
                                    // //         child: DropdownButton<String>(
                                    // //           elevation: 0,
                                    // //           focusColor:
                                    // //               AppColors.secondaryColor,
                                    // //           value:
                                    // //               controller.chosenValue.value,
                                    // //           iconEnabledColor:
                                    // //               AppColors.primaryColor,
                                    // //           items: <String>[
                                    // //             'English',
                                    // //             'አማርኛ',
                                    // //           ].map<DropdownMenuItem<String>>(
                                    // //               (String value) {
                                    // //             return DropdownMenuItem<String>(
                                    // //               value: value,
                                    // //               child: Center(
                                    // //                 child: Text(
                                    // //                   value,
                                    // //                   style: TextStyle(
                                    // //                       fontSize: 17,
                                    // //                       color: AppColors
                                    // //                           .secondaryColor),
                                    // //                 ),
                                    // //               ),
                                    // //             );
                                    // //           }).toList(),
                                    // //           onChanged: (value) {
                                    // //             controller.chosenValue.value =
                                    // //                 value!;
                                    // //             // controller.updateLanguage();
                                    // //             if (controller
                                    // //                     .chosenValue.value ==
                                    // //                 "English") {
                                    // //               Storage.storage
                                    // //                   .write('lang', 'English');
                                    // //               controller.langCtrl
                                    // //                   .changeLocale('English');
                                    // //             } else if (controller
                                    // //                     .chosenValue.value ==
                                    // //                 "አማርኛ") {
                                    // //               Storage.storage
                                    // //                   .write('lang', 'አማርኛ');
                                    // //               controller.langCtrl
                                    // //                   .changeLocale('አማርኛ');
                                    // //             }
                                    // //           },
                                    // //         ),
                                    // //       ),
                                    // //     ),
                                    // //   ),
                                    // // ),
                                  ],
                                ),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.3,
                                ),
                                Padding(
                                    padding: EdgeInsets.only(
                                        bottom: MediaQuery.of(context)
                                            .viewInsets
                                            .bottom))
                              ],
                            ),
                          ),
                        ),
                      )))
            ],
          ),
        ));
  }
}
