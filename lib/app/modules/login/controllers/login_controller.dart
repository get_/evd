import 'dart:convert';

import 'package:evd_mobile/app/modules/batch/controllers/batch_controller.dart';
import 'package:evd_mobile/app/modules/bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import 'package:evd_mobile/app/modules/home/controllers/home_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:logger/logger.dart';

import '../../../common/storage/storage.dart';
import '../../../common/util/localization.dart';
import '../../../data/api_helper.dart';
import '../../../routes/app_pages.dart';
import '../../distributor/controllers/accountholder_list_controller.dart';
import '../user_login_model.dart';

class LoginController extends GetxController {
  final count = 0.obs;
  TextEditingController userPasswordController = TextEditingController();
  TextEditingController userPhoneController = TextEditingController();
  final ApiHelper _apiHelper = Get.find();
  RxBool passwordVisible = true.obs;
  RxString chosenValue = 'EN'.obs;
  RxBool isAmharic = false.obs;
  GetStorage cc = GetStorage();
  var langCtrl = Get.put(LocaleString());

  // @override
  // void onInit() {
  //   Storage.storage.writeIfNull('lang', chosenValue.value);
  //   Future.delayed(Duration.zero, () async {
  //     getlang();
  //   });
  //   super.onInit();
  // }

  // getlang() {
  //   Storage.storage.read("lang") == "English"
  //       ? langCtrl.changeLocale('English')
  //       : langCtrl.changeLocale('አማርኛ');
  // }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
  loginhandler() {
    DistributerModel data = DistributerModel(
        un: userPhoneController.text.toString(),
        pwd: userPasswordController.text.toString());
    _apiHelper.login(data).futureValue(
      (dynamic value) {
        Map<String, dynamic> res = jsonDecode(value);
        Storage.storage.write('token', res['token']);
        Logger().i('USER LOGGED....');
        userPasswordController.clear();
        userPhoneController.clear();
        // cc.write('lang', chosenValue.value);
        // var x = Get.put<AccountholderController>(AccountholderController());
        // var y = Get.put<BatchController>(BatchController());
        Get.put<HomeController>(HomeController());
        Get.put<AccountholderController>(AccountholderController());
        Get.put<LocaleString>(LocaleString());
        Get.put<BatchController>(BatchController());
        Get.put<BluetoothconnectionController>(BluetoothconnectionController());
        // x.getAccountHolder();
        // y.getFaceValueCount(true);
        Get.offAllNamed(Routes.BATCH);
      },
      retryFunction: loginhandler,
    );
  }
}
