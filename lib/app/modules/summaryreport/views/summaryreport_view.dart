import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/constants.dart';
import '../../../common/util/exports.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../../widgets/custom_drawer_widget.dart';
import '../controllers/summaryreport_controller.dart';

class SummaryreportView extends GetView<SummaryreportController> {
  final homeController = Get.find<HomeController>();

  var controller = Get.put(SummaryreportController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () => controller.refreshfetchSummaryReport(),
              icon: Icon(Icons.refresh))
        ],
        title: Align(
            alignment: Alignment.centerLeft, child: Text('Summary Report'.tr)),
        backgroundColor: AppColors.primaryColor,
      ),
      drawer: CustomDrawerWidget(controller: homeController),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.08,
            ),
            width: MediaQuery.of(context).size.width * 0.8,
            height: MediaQuery.of(context).size.height * 0.24,
            decoration: BoxDecoration(
                color: AppColors.primaryColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15))),
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            MediaQuery.of(context).size.width * 0.05, 0, 0, 5),
                        child: Text(
                          'Total Balance'.tr,
                          style: TextStyle(
                              color: AppColors.white,
                              fontSize: Dimens.fontSize18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.05,
                          ),
                          Icon(
                            Icons.account_balance_wallet,
                            color: AppColors.whiteColor,
                          ),
                          Text(
                            '  ${Utils.numberFormat(controller.account.ds.balance)}',
                            style: TextStyle(
                                color: AppColors.whiteColor, fontSize: 25),
                          ),
                          Text(' BIRR'.tr,
                              style: TextStyle(color: AppColors.secondaryColor))
                        ],
                      ),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    Get.toNamed(Routes.DEPOSIT);
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.09,
                    color: AppColors.secondaryColor,
                    child: Center(
                      child: Text('Deposit'.tr,
                          style: TextStyle(
                              color: AppColors.whiteColor,
                              fontSize: Dimens.fontSize20)),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.width * 0.06, left: 10),
            child: Text('Sales'.tr,
                style: TextStyle(
                    fontSize: Dimens.fontSize20, fontWeight: FontWeight.bold)),
          ),
          controller.obx((state) => Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.380,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    physics: ScrollPhysics(),
                    padding: EdgeInsets.all(10),
                    shrinkWrap: true,
                    itemCount: Constants.SUMMARIES.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Get.toNamed(Routes.DAILYSALES, arguments: {
                            'title': Constants.SUMMARIES[index].tr,
                            'data': controller.xx[index]
                          });
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              right: MediaQuery.of(context).size.width * 0.03),
                          width: MediaQuery.of(context).size.width * 0.43,
                          // height MediaQuery.of(context).size.width * 0.06: MediaQuery.of(context).size.height * 0.3,
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 0), // changes position of shadow
                                ),
                              ],
                              color: (index % 2) == 0
                                  ? AppColors.primaryColor
                                  : Colors.white70,
                              borderRadius: BorderRadius.all(Radius.circular(
                                  MediaQuery.of(context).size.width * 0.03))),
                          child: Stack(children: [
                            Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Flexible(
                                      child: Text(
                                    Constants.SUMMARIES[index].tr,
                                    style: TextStyle(
                                        fontSize: Dimens.fontSize22,
                                        fontWeight: FontWeight.bold,
                                        color: (index % 2) == 0
                                            ? AppColors.whiteColor
                                            : AppColors.primaryColor),
                                  )),
                                  Flexible(
                                      child: Text(
                                    index == 0
                                        ? state!.daily.isEmpty
                                            ? '0.00' + ' BIRR'.tr
                                            : Utils.numberFormat(controller
                                                    .dailyTotal.value) +
                                                ' BIRR'.tr
                                        : index == 1
                                            ? state!.weekly.isEmpty
                                                ? '0.00' + ' BIRR'.tr
                                                : Utils.numberFormat(controller
                                                        .weeklyTotal.value) +
                                                    ' BIRR'.tr
                                            : state!.monthly.isEmpty
                                                ? '0.00' + ' BIRR'.tr
                                                : Utils.numberFormat(controller
                                                        .monthlyTotal.value) +
                                                    ' BIRR'.tr,
                                    style: TextStyle(
                                        color: AppColors.secondaryColor,
                                        fontSize: Dimens.fontSize20,
                                        fontWeight: FontWeight.bold),
                                  )),
                                  Container(
                                    child: Text(
                                      'Number of Cards'.tr,
                                      style: TextStyle(
                                        color: AppColors.secondaryColor,
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                      child: Text(
                                    index == 0
                                        ? state.daily.isEmpty
                                            ? '0'
                                            : controller.dailyCardTotal.value
                                                .toInt()
                                                .toString()
                                        : index == 1
                                            ? state.weekly.isEmpty
                                                ? '0'
                                                : controller
                                                    .weeeklyCardTotal.value
                                                    .toInt()
                                                    .toString()
                                            : state.monthly.isEmpty
                                                ? '0'
                                                : controller
                                                    .monthlyCardTotal.value
                                                    .toInt()
                                                    .toString(),
                                    style: TextStyle(
                                        color: (index % 2) == 0
                                            ? AppColors.whiteColor
                                            : AppColors.primaryColor,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  )),
                                ],
                              ),
                            ),
                          ]),
                        ),
                      );
                    }),
              ))
        ],
      ),
    );
  }
}
