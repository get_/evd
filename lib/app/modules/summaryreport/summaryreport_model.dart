// To parse this JSON data, do
//
//     final ReportModel = ReportModelFromJson(jsonString);

import 'dart:convert';

ReportModel ReportModelFromJson(String str) => ReportModel.fromJson(json.decode(str));

String ReportModelToJson(ReportModel data) => json.encode(data.toJson());

class ReportModel {
    ReportModel({
        required this.daily,
        required this.weekly,
        required this.monthly,
    });

    List<Daily> daily;
    List<Daily> weekly;
    List<Daily> monthly;

    factory ReportModel.fromJson(Map<String, dynamic> json) => ReportModel(
        daily: List<Daily>.from(json["daily"].map((x) => Daily.fromJson(x))),
        weekly: List<Daily>.from(json["weekly"].map((x) => Daily.fromJson(x))),
        monthly: List<Daily>.from(json["monthly"].map((x) => Daily.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "daily": List<dynamic>.from(daily.map((x) => x.toJson())),
        "weekly": List<dynamic>.from(weekly.map((x) => x.toJson())),
        "monthly": List<dynamic>.from(monthly.map((x) => x.toJson())),
    };
}

class Daily {
    Daily({
        required this.faceValue,
        required this.numberOfCards,
        required this.balance,
    });

    int faceValue;
    double numberOfCards;
    double balance;

    factory Daily.fromJson(Map<String, dynamic> json) => Daily(
        faceValue: json["face_value"],
        numberOfCards: json["number_of_cards"],
        balance: json["balance"],
    );

    Map<String, dynamic> toJson() => {
        "face_value": faceValue,
        "number_of_cards": numberOfCards,
        "balance": balance,
    };
}
