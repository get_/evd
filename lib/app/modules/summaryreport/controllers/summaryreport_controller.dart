import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../data/api_helper.dart';
import '../../batch/controllers/batch_controller.dart';
import '../../distributor/controllers/accountholder_list_controller.dart';
import '../summaryreport_model.dart';

class SummaryreportController extends GetxController
    with StateMixin<ReportModel> {
  final ApiHelper _apiHelper = Get.find();
  var account = Get.put(AccountholderController());
  var balace = Get.put(BatchController());
  final count = 0.obs;
  @override
  void onReady() {
    super.onReady();
    fetchSummaryReport();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  List<Daily> daily = [Daily(faceValue: 0, numberOfCards: 0.0, balance: 0)].obs;
  List<Daily> weekly =
      [Daily(faceValue: 0, numberOfCards: 0.0, balance: 0)].obs;
  List<Daily> monthly =
      [Daily(faceValue: 0, numberOfCards: 0.0, balance: 0)].obs;
  List<List<Daily>> xx = [];
  ReportModel? reportModel;

  fetchSummaryReport() {
    daily.clear();
    weekly.clear();
    monthly.clear();
    ReportModel(daily: daily, weekly: weekly, monthly: monthly);
    change(null, status: RxStatus.empty());
    _apiHelper.fetchSummaryReport().futureValue(
      (dynamic value) {
        change(ReportModelFromJson(value), status: RxStatus.success());
        reportModel = ReportModelFromJson(value);
        daily = reportModel!.daily;
        monthly = reportModel!.monthly;
        weekly = reportModel!.weekly;
        totalDaily();
        totalWeekly();
        totalMonthly();
        xx = [daily, weekly, monthly];
        // print(ReportModelFromJson(value).toString());
        Logger().d(ReportModelFromJson(value).toString());
      },
      retryFunction: fetchSummaryReport,
    );
  }

  refreshfetchSummaryReport() {
    ReportModel(daily: daily, weekly: weekly, monthly: monthly);
    change(null, status: RxStatus.empty());
    _apiHelper.fetchSummaryReport().futureValue(
      (dynamic value) {
        change(ReportModelFromJson(value), status: RxStatus.success());
        reportModel = ReportModelFromJson(value);
        Logger().d(ReportModelFromJson(value).toString());
      },
      retryFunction: fetchSummaryReport,
    );
  }

  //daily total
  RxDouble dailyTotal = 0.0.obs;
  RxDouble dailyCardTotal = 0.0.obs;

  double totalDaily() {
    for (var i = 0; i < daily.length; i++) {
      dailyTotal.value += daily[i].balance;
      dailyCardTotal.value += daily[i].numberOfCards;
    }
    return dailyTotal.value;
  }

  //week total
  RxDouble weeklyTotal = 0.0.obs;
  RxDouble weeeklyCardTotal = 0.0.obs;

  double totalWeekly() {
    for (var i = 0; i < weekly.length; i++) {
      weeklyTotal.value += weekly[i].balance;
      weeeklyCardTotal.value += weekly[i].numberOfCards;
    }
    return weeklyTotal.value;
  }

  //month total
  RxDouble monthlyTotal = 0.0.obs;
  RxDouble monthlyCardTotal = 0.0.obs;

  double totalMonthly() {
    for (var i = 0; i < monthly.length; i++) {
      monthlyTotal.value += monthly[i].balance;
      monthlyCardTotal.value += monthly[i].numberOfCards;
    }
    return monthlyTotal.value;
  }
}
