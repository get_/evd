import 'package:get/get.dart';

import '../controllers/summaryreport_controller.dart';

class SummaryreportBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SummaryreportController>(
      () => SummaryreportController(),
    );
  }
}
