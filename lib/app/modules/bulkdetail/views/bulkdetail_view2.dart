// ignore_for_file: unrelated_type_equality_checks, curly_braces_in_flow_control_structures, must_be_immutable

import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../common/util/exports.dart';
import '../../bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/bulkdetail_controller.dart';

class BulkdetailView extends GetView<BulkdetailController> {
  final homeController = Get.find<HomeController>();
  var bController = Get.find<BluetoothconnectionController>();

// controller.selectedVochureochureMap
//                                                 .forEach((key, value) {
//                                               Logger().i(key);
//                                               controller.pv.value.data
//                                                   .forEach((element) {
//                                                 Logger().i(element.serialNo);
//                                                 controller.checkedCounter
//                                                     .clear();
//                                                 if (newvalue == true &&
//                                                     element.reprintCount == 3 &&
//                                                     element.serialNo
//                                                             .toString() ==
//                                                         key.toString()) {
//                                                   Logger().i(element.serialNo);
//                                                   controller
//                                                       .selectedVochureochureMap
//                                                       .remove(key);
//                                                 } else {
//                                                   controller.checkedCounter
//                                                       .clear();
//                                                   controller
//                                                       .selectedVochureochureMap
//                                                       .forEach((key, value) {
//                                                     key = newvalue;
//                                                   });
//                                                   controller.checkedCounter
//                                                       .addAll(controller
//                                                           .selectedVochureochureMap
//                                                           .values);
//                                                 }
//                                               });
//                                             });

  @override
  final BulkdetailController controller = Get.put(BulkdetailController());

  Future<void> builBottomModalSheet(BuildContext context) {
    return showModalBottomSheet<void>(
        context: context,
        isDismissible: true,
        builder: (BuildContext context) {
          return Container(
              height: 250,
              color: AppColors.whiteColor,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: ListView(
                  padding: EdgeInsets.all(10),
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Device'.tr + ':',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Obx(() => bController.devices.isEmpty
                            ? Center(child: Text('No Device'.tr))
                            : DropdownButton<BluetoothDevice>(
                                hint: Text('select device'.tr),
                                focusColor: Colors.white,
                                value: bController.device.value.name == ''
                                    ? null
                                    : bController.device.value,
                                style: TextStyle(color: Colors.white),
                                iconEnabledColor: Colors.black,
                                items: bController.devices
                                    .map<DropdownMenuItem<BluetoothDevice>>(
                                        (product) {
                                  return DropdownMenuItem<BluetoothDevice>(
                                    value: product,
                                    child: Text(
                                      product.name!,
                                      style: TextStyle(color: Colors.orange),
                                    ),
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  bController.device.value = value!;
                                },
                              )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: AppColors.primaryColor),
                            onPressed: () {
                              bController.initPlatformState();
                            },
                            child: Text(
                              'Refresh'.tr,
                              style: TextStyle(color: AppColors.whiteColor),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Obx(() => Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: bController.connected.value
                                        ? Colors.red
                                        : Colors.green),
                                onPressed: bController.connected.value
                                    ? bController.disconnect
                                    : bController.connect,
                                child: bController.connecting.value
                                    ? Container(
                                        width: 100,
                                        height: 20,
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: CircularProgressIndicator(
                                                color: AppColors.whiteColor,
                                              ),
                                            ),
                                            Expanded(
                                              child: Text(
                                                'loading...',
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color:
                                                        AppColors.whiteColor),
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    : Text(
                                        bController.connected.value
                                            ? 'Disconnect'.tr
                                            : 'Connect'.tr,
                                        style: TextStyle(color: Colors.white),
                                      ),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ));
        });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Align(
              alignment: Alignment.centerLeft, child: Text('Reprint Card'.tr)),
          centerTitle: true,
          backgroundColor: AppColors.primaryColor,
          elevation: 0,
          actions: [
            IconButton(
              onPressed: () {
                builBottomModalSheet(context);
              },
              icon: Icon(
                Icons.bluetooth,
                color: AppColors.whiteColor,
                size: Dimens.fontSize24,
              ),
            )
          ],
        ),
        body: Obx(() => controller.isloading.value ||
                controller.pv.value.data.isEmpty
            ? Center(child: CircularProgressIndicator())
            : controller.pv.value.total == 0
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image(image: AssetImage(AppImages.nodata)),
                        RichText(
                          text: TextSpan(
                            text: 'Something went Wrong'.tr,
                            style: TextStyle(fontSize: 18, color: Colors.black),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'please try again'.tr,
                            style: TextStyle(fontSize: Dimens.fontSize18),
                          ),
                        ),
                      ],
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Obx(() => controller.checkedCounter.isEmpty
                                    ? Material(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5),
                                        child: InkWell(
                                          hoverColor: Colors.orange,
                                          splashColor: Colors.red,
                                          focusColor: Colors.yellow,
                                          highlightColor: Colors.purple,
                                          onTap: null,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Container(
                                            width: 100,
                                            height: 30,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                            ),
                                            alignment: Alignment.center,
                                            child: Text(
                                                'Reprint'.tr.toUpperCase()),
                                          ),
                                        ),
                                      )
                                    : Material(
                                        color: Colors.amber,
                                        borderRadius: BorderRadius.circular(5),
                                        child: InkWell(
                                          onTap: () {
                                            controller.selectAndReprint();
                                          },
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Container(
                                            width: 100,
                                            height: 30,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                            ),
                                            alignment: Alignment.center,
                                            child: Text(
                                                'Reprint'.tr.toUpperCase()),
                                          ),
                                        ),
                                      ))
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Obx(() => Checkbox(
                                          activeColor: AppColors.primaryColor,
                                          value: controller.isAllChecked.value,
                                          onChanged: (newvalue) {
                                            Logger().d(newvalue);
                                            controller.isAllChecked.value =
                                                newvalue!;

                                            controller.selectedVochureochureMap
                                                .forEach((key, value) {
                                              controller
                                                      .selectedVochureochureMap[
                                                  key] = newvalue;
                                            });
                                            var validSerials = controller
                                                .pv.value.data
                                                .where((element) =>
                                                    int.parse(
                                                        element.reprintCount) <
                                                    3)
                                                .map((e) => e.serialNo)
                                                .toSet();
                                            if (newvalue == true) {
                                              controller.checkedCounter.clear();
                                              controller.checkedCounter.addAll(
                                                controller
                                                    .selectedVochureochureMap
                                                    .keys
                                                    .where(
                                                  (element) =>
                                                      validSerials.contains(
                                                    element,
                                                  ),
                                                ),
                                              );
                                            } else if (newvalue == false) {
                                              controller.checkedCounter.clear();
                                            }
                                          })),
                                      Text('Select All'.tr),
                                    ],
                                  ),
                                  Obx(() {
                                    return Flexible(
                                      child: Text(
                                          '${controller.checkedCounter.length} ' +
                                              'selected'.tr +
                                              ' ${controller.pv.value.data.length} ' +
                                              'Cards'.tr),
                                    );
                                  }),
                                ],
                              ),
                            ),
                            Obx(() {
                              return ListView.builder(
                                  physics: ScrollPhysics(),
                                  shrinkWrap: true,
                                  scrollDirection: Axis.vertical,
                                  itemCount: controller.pv.value.total,
                                  itemBuilder: (context, i) {
                                    return Obx(() => Card(
                                          color: int.parse(controller.pv.value
                                                      .data[i].reprintCount) ==
                                                  3
                                              ? Colors.white60
                                              : null,
                                          elevation: 5,
                                          child: ListTile(
                                            leading: Checkbox(
                                                value: int.parse(controller
                                                            .pv
                                                            .value
                                                            .data[i]
                                                            .reprintCount) ==
                                                        3
                                                    ? true
                                                    : controller
                                                            .selectedVochureochureMap[
                                                        controller.pv.value
                                                            .data[i].serialNo],
                                                activeColor: int.parse(controller
                                                            .pv
                                                            .value
                                                            .data[i]
                                                            .reprintCount) ==
                                                        3
                                                    ? Colors.grey.shade400
                                                    : AppColors.primaryColor,
                                                onChanged: (newValue) {
                                                  int.parse(controller
                                                              .pv
                                                              .value
                                                              .data[i]
                                                              .reprintCount) ==
                                                          3
                                                      ? {
                                                          newValue = null,
                                                        }
                                                      : newValue;
                                                  controller
                                                          .selectedVochureochureMap[
                                                      controller
                                                          .pv
                                                          .value
                                                          .data[i]
                                                          .serialNo] = newValue;

                                                  if (newValue == false) {
                                                    controller.isAllChecked
                                                        .value = false;

                                                    controller.checkedCounter
                                                        .removeAt(controller
                                                                .checkedCounter
                                                                .length -
                                                            1);
                                                  } else if (newValue == true) {
                                                    controller.checkedCounter
                                                        .add(true);
                                                  }
                                                  if (!controller
                                                      .selectedVochureochureMap
                                                      .containsValue(false)) {
                                                    controller.isAllChecked
                                                        .value = true;
                                                  }

                                                  Logger().d(newValue);
                                                  Logger().i(controller.pv.value
                                                      .data[i].serialNo);
                                                }),
                                            //  : null,
                                            title: Text.rich(TextSpan(
                                                text: 'SN: ',
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                children: <InlineSpan>[
                                                  TextSpan(
                                                    text: controller.pv.value
                                                        .data[i].serialNo,
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.normal),
                                                  )
                                                ])),
                                            subtitle: Obx(() => Text(
                                                "Reprint Count: " +
                                                    controller.pv.value.data[i]
                                                        .reprintCount)),
                                            trailing: CircleAvatar(
                                              child: Text(
                                                controller
                                                    .pv.value.data[i].faceValue
                                                    .toString(),
                                                style: TextStyle(
                                                    color:
                                                        AppColors.whiteColor),
                                              ),
                                              backgroundColor: Colors.amber,
                                            ),
                                          ),
                                        ));
                                  });
                            })
                          ],
                        ),
                      ),
                    ),
                  )),
      );

  // contentBox(context, id, select) {
  //   return Stack(
  //     children: <Widget>[
  //       Container(
  //         padding:
  //             EdgeInsets.only(left: 20, top: 45 + 20, right: 20, bottom: 20),
  //         margin: EdgeInsets.only(top: 45),
  //         decoration: BoxDecoration(
  //             shape: BoxShape.rectangle,
  //             color: Colors.white,
  //             borderRadius: BorderRadius.circular(20),
  //             boxShadow: [
  //               BoxShadow(
  //                   color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
  //             ]),
  //         child: Column(
  //           mainAxisSize: MainAxisSize.min,
  //           children: <Widget>[
  //             Text(
  //               'Confirm print'.tr,
  //               style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
  //             ),
  //             SizedBox(
  //               height: 15,
  //             ),
  //             Flexible(
  //               child: Text(
  //                 'Reprint'.tr + '?',
  //                 style: TextStyle(fontSize: 14),
  //                 textAlign: TextAlign.center,
  //               ),
  //             ),
  //             SizedBox(
  //               height: 22,
  //             ),
  //             Row(
  //               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //               children: [
  //                 Align(
  //                   alignment: Alignment.bottomLeft,
  //                   child: TextButton(
  //                       onPressed: () {
  //                         if (select) {
  //                           controller.reprint(id);
  //                         } else {
  //                           controller.selectAndReprint();
  //                         }

  //                         Get.back();
  //                       },
  //                       child: Text(
  //                         'Confirm'.tr,
  //                         style: TextStyle(
  //                             fontSize: Dimens.fontSize18,
  //                             color: AppColors.primaryColor),
  //                       )),
  //                 ),
  //                 Align(
  //                   alignment: Alignment.bottomRight,
  //                   child: TextButton(
  //                       onPressed: () {
  //                         Get.back();
  //                       },
  //                       child: Text(
  //                         'Cancel'.tr,
  //                         style: TextStyle(
  //                             fontSize: Dimens.fontSize18,
  //                             color: AppColors.primaryColor),
  //                       )),
  //                 ),
  //               ],
  //             ),
  //           ],
  //         ),
  //       ),
  //       Positioned(
  //         left: 20,
  //         right: 20,
  //         child: CircleAvatar(
  //             backgroundColor: AppColors.secondaryColor,
  //             radius: 45,
  //             child: ClipRRect(
  //               borderRadius: BorderRadius.all(Radius.circular(45)),
  //               child: Icon(Icons.print,
  //                   color: AppColors.whiteColor,
  //                   size: MediaQuery.of(context).size.width * 0.2),
  //             )),
  //       ),
  //     ],
  //   );
  // }

}
