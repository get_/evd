import 'dart:convert';

BulkPrints bulkPrintsFromJson(String str) =>
    BulkPrints.fromJson(json.decode(str));

String bulkPrintsToJson(BulkPrints data) => json.encode(data.toJson());
List<Datum> datumFromJson(String str) =>
    List<Datum>.from(json.decode(str).map((x) => Datum.fromJson(x)));

class BulkPrints {
  BulkPrints({
    required this.data,
    required this.total,
  });

  List<Datum> data;
  int total;

  factory BulkPrints.fromJson(Map<String, dynamic> json) => BulkPrints(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class Datum {
  Datum(
      {required this.id,
      required this.printId,
      required this.voucherId,
      required this.faceValue,
      required this.regDate,
      required this.serialNo,
      required this.reprintCount});
  String id;
  String printId;
  String voucherId;
  int faceValue;
  int regDate;
  String serialNo;
  String reprintCount;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        printId: json["print_id"],
        voucherId: json["voucher_id"],
        faceValue: json["face_value"],
        regDate: json["reg_date"],
        serialNo: json["serial_no"],
        reprintCount: json["reprint_count"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "print_id": printId,
        "voucher_id": voucherId,
        "face_value": faceValue,
        "reg_date": regDate,
        "serial_no": serialNo,
        "reprint_count": reprintCount,
      };
}
