import 'dart:convert';

import 'package:evd_mobile/app/modules/bulkdetail/select_print_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../data/api_helper.dart';
import '../../bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import '../../distributor/controllers/accountholder_list_controller.dart';
import '../../printpage/api_error_model.dart';
import '../../voucherhistory/bulk_reprint_model.dart';
import '../../widgets/toast.dart';
import '../bulk_detail_model.dart';
import '../single_reprint_responce_model.dart';

class BulkdetailController extends GetxController with StateMixin<BulkPrints> {
  String arg = Get.arguments;
  String printId = '';
  final count = 0.obs;

  final ScrollController scrollController = ScrollController();
  final ApiHelper _apiHelper = Get.find();
  RxBool isloading = true.obs;
  Rx<BulkPrints> pv = BulkPrints(data: [], total: 0).obs;
  RxList<Datum> detail_list = <Datum>[].obs;
  final BluetoothconnectionController blue =
      Get.find<BluetoothconnectionController>();
  SingleReprintResponce result = SingleReprintResponce(
      id: "",
      batchId: "",
      batchNo: "",
      serialNo: "",
      faceValue: 0,
      isPrint: true,
      reprintCount: 0,
      operatorId: "",
      regDate: 0);
  RxMap selectedVochureochureMap = {}.obs;
  RxList holdTrue = [].obs;
  RxMap hold = {}.obs;
  RxBool isAllChecked = false.obs;
  RxList checkedCounter = [].obs;
  int page = 0;
  int maxPage = 0;
  @override
  void onInit() {
    printId = arg;
    getIntialVouchers();
    scrollController.addListener(pagination);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    getIntialVouchers();
  }

  @override
  void onClose() {
    scrollController.removeListener(pagination);
  }

  Future<void> getIntialVouchers() async {
    isloading.value = true;
    change(null, status: RxStatus.empty());
    Logger().d(printId);
    _apiHelper.getBulkPrintVouchers(printId, 0).futureValue(
      (value) async {
        Logger().d(value.toString());
        pv.value = bulkPrintsFromJson(value);
        isloading.value = false;
        detail_list.value = pv.value.data;
        for (var i = 0; i < pv.value.total; i++) {
          var x = bulkPrintsFromJson(value).data[i];
          hold[x.id] = false;
          selectedVochureochureMap[x.serialNo] = false;
          if (int.parse(x.reprintCount) == 3) {
            counter.value++;
          }
        }
        isloading.value = false;
        Logger().d(counter);
        pv.value = bulkPrintsFromJson(value);

        pv.value.data
            .sort((date1, date2) => date2.regDate.compareTo(date1.regDate));

        change(pv.value, status: RxStatus.success());
      },
    );
    page++;
  }

  Future<void> getVouchers() async {
    isloading.value = true;
    change(null, status: RxStatus.empty());
    if (maxPage == 0) {
      _apiHelper.getBulkPrintVouchers(printId, page).then(
        (value) async {
          BulkPrints bulkprints = bulkPrintsFromJson(value.body);
          if (bulkprints.data.isNotEmpty) {
            pv.value.data.addAll(bulkprints.data);
            isloading.value = false;
            page++;
          } else {
            maxPage = page;
          }
          pv.value.data
              .sort((date1, date2) => date2.regDate.compareTo(date1.regDate));

          change(pv.value, status: RxStatus.success());
          Logger().d("length after" + pv.value.data.length.toString());
        },
      );
    } else {
      toast("no more data");
      change(pv.value, status: RxStatus.success());
    }
  }

  void pagination() {
    if (scrollController.position.atEdge &&
        state != null &&
        pv.value.total != pv.value.data.length &&
        !isloading.value) {
      getVouchers();
    }
  }

  AccountholderController account = Get.put(AccountholderController());
  List<SelectPrintVoucher> list_bulk_voucher = [];

  Future<void> reprint(id) async {
    bool? isOn = await blue.bluetooth.isOn;
    bool? isConnected = await blue.bluetooth.isConnected;

    if (!isOn!) {
      blue.show('Turn on Bluetooth First'.tr);
    } else if (!isConnected!) {
      blue.show('No Bluetooth Connection'.tr);
    } else {
      _apiHelper.singleReprint(id).then((value) async {
        if (value.statusCode == 200) {
          result = singleReprintResponceFromJson(value.body);
          blue.startSingleRePrint(result, account.ds.agentUsername)!;
        } else if (value.statusCode == 400) {
          ApiError msg = apiErrorFromJson(value.body);
          toast(msg.msgs[0]);
        }
      });
    }
  }

  var selectedId = [];
  var printed = [];
  RxInt counter = 0.obs;
  selectAndReprint() async {
    // checkedCounter.value = [];
    var selectedSerial = [];
    selectedId.clear();
    selectedVochureochureMap.entries.forEach((element) {
      element.value ? selectedSerial.add(element.key) : null;
    });
    detail_list.forEach((element) {
      if (selectedSerial.contains(element.serialNo)) {
        int.parse(element.reprintCount) < 3
            ? selectedId.add(element.voucherId)
            : null;
      }
    });

    bool? isOn = await blue.bluetooth.isOn;
    bool? isConnected = await blue.bluetooth.isConnected;
    if (!isOn!) {
      blue.show('Turn on Bluetooth First'.tr);
    } else if (!isConnected!) {
      blue.show('No Bluetooth Connection'.tr);
    } else {
    Logger().d(selectedId);
    _apiHelper.selectReprint(selectedId).then((value) {
      onInit();
      if (value.statusCode == 200) {
        Logger().d(value.body);
        list_bulk_voucher = selectPrintVoucherFromJson(value.body);
        for (var voucher in pv.value.data) {
          list_bulk_voucher.forEach((element) {
            if (voucher.serialNo == element.serialNo) {
              Logger().d(voucher.reprintCount);
              Logger().d(element.reprintCount);
              voucher.reprintCount = (element.reprintCount + 1).toString();
              Logger().d(voucher.reprintCount);
              Logger().d(element.reprintCount);
            }
          });
          if (int.parse(voucher.reprintCount) == 3) {
            counter++;
          }
        }
        blue.startSelectRePrint(list_bulk_voucher, account.ds.agentUsername);
        printed.addAll(selectedId);
        // change(pv.value, status: RxStatus.success());
      } else {
        var err = apiErrorFromJson(value.body);
        toast(err.msgs[0]);
      }
    });
    }
  }

  getCounter(id) {
    int counter = 0;
    for (int i = 0; i <= printed.length; i++) {
      if (printed[i] == id) {
        counter++;
      }
    }
    return counter;
  }
}
