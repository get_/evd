import 'package:get/get.dart';

import '../controllers/bulkdetail_controller.dart';

class BulkdetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BulkdetailController>(
      () => BulkdetailController(),
    );
  }
}
