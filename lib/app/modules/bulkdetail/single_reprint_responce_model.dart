// To parse this JSON data, do
//
//     final singleReprintResponce = singleReprintResponceFromJson(jsonString);

import 'dart:convert';

SingleReprintResponce singleReprintResponceFromJson(String str) =>
    SingleReprintResponce.fromJson(json.decode(str));

String singleReprintResponceToJson(SingleReprintResponce data) =>
    json.encode(data.toJson());

class SingleReprintResponce {
  SingleReprintResponce({
    required this.id,
    required this.batchId,
    required this.batchNo,
    required this.serialNo,
    required this.faceValue,
    required this.isPrint,
    required this.reprintCount,
    required this.operatorId,
    required this.regDate,
    this.voucherNumber,
  });

  String id;
  String batchId;
  String batchNo;
  String serialNo;
  int faceValue;
  bool isPrint;
  int reprintCount;
  String operatorId;
  int regDate;
  dynamic voucherNumber;

  factory SingleReprintResponce.fromJson(Map<String, dynamic> json) =>
      SingleReprintResponce(
        id: json["id"],
        batchId: json["batch_id"],
        batchNo: json["batch_no"],
        serialNo: json["serial_no"],
        faceValue: json["face_value"],
        isPrint: json["is_print"],
        reprintCount: json["reprint_count"],
        operatorId: json["operator_id"],
        regDate: json["reg_date"],
        voucherNumber: json["voucher_number"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "batch_id": batchId,
        "batch_no": batchNo,
        "serial_no": serialNo,
        "face_value": faceValue,
        "is_print": isPrint,
        "reprint_count": reprintCount,
        "operator_id": operatorId,
        "reg_date": regDate,
        "voucher_number": voucherNumber,
      };
}
