import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:evd_mobile/app/modules/home/controllers/home_controller.dart';
import 'package:evd_mobile/app/modules/widgets/custom_drawer_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/deposit_controller.dart';

class DepositView extends GetView<DepositController> {
  HomeController home_controller = HomeController();
  final controller = Get.put(DepositController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Align(alignment: Alignment.centerLeft, child: Text('Deposit'.tr)),
        centerTitle: true,
        backgroundColor: AppColors.primaryColor,
        actions: [
          IconButton(
              onPressed: () => controller.getDeposits(),
              icon: Icon(Icons.refresh))
        ],
      ),
      drawer: CustomDrawerWidget(controller: home_controller),
      body: Center(
          child: Container(
        height: MediaQuery.of(context).size.height,
        child: ListView(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          children: [
            Padding(
                padding: const EdgeInsets.all(28.0),
                child: controller.obx((state) => state!.data.isEmpty
                    ? Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image(image: AssetImage(AppImages.nodata)),
                            Text(
                              'No deposit found'.tr,
                              style:
                                  TextStyle(fontSize: 18, color: Colors.black),
                            ),
                          ],
                        ),
                      )
                    : ListView.builder(
                        shrinkWrap: true,
                        itemCount: state.total,
                        itemBuilder: (context, index) => Card(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  ListTile(
                                    leading: Icon(
                                      Icons.monetization_on,
                                      size: 45,
                                      color: AppColors.secondaryColor,
                                    ),
                                    trailing: Text(Utils.convertToDate(
                                        DateTime.fromMillisecondsSinceEpoch(
                                                (state.data[index].regDate *
                                                        1000)
                                                    .toInt())
                                            .toString())),
                                    title: Text(
                                      Utils.numberFormat(
                                              state.data[index].amount) +
                                          ' BIRR'.tr,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    subtitle: Text('Amount'.tr),
                                  ),
                                ],
                              ),
                            ))))
          ],
        ),
      )),
    );
  }
}
