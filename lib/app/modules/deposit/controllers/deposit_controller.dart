// ignore_for_file: unnecessary_new

import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:evd_mobile/app/data/api_helper.dart';
import 'package:evd_mobile/app/modules/deposit/deposit_model.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class DepositController extends GetxController with StateMixin<Deposits> {
  final ApiHelper _apiHelper = Get.find();
  final count = 0.obs;

  @override
  void onReady() {
    super.onReady();
    if (Storage.storage.hasData('token')) {
      getDeposits();
    } else {}
  }

  @override
  void onClose() {}
  void increment() => count.value++;
  getDeposits() {
    Logger().d('get deposit...............');
    change(null, status: RxStatus.empty());
    _apiHelper.getDeposit().futureValue(
      (dynamic value) {
        // var xx = depositFromJson(value);
        change(depositFromJson(value), status: RxStatus.success());
      },
      retryFunction: getDeposits,
    );
  }

  String returnbirr(String birr) {
    return '';
  }
}
