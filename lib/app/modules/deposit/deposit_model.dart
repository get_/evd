// To parse this JSON data, do
//
//     final deposit = depositFromJson(jsonString);

import 'dart:convert';

Deposits depositFromJson(String str) => Deposits.fromJson(json.decode(str));

String depositToJson(Deposits data) => json.encode(data.toJson());

class Deposits {
  Deposits({
    required this.data,
    required this.total,
  });

  List<Deposit> data;
  int total;

  factory Deposits.fromJson(Map<String, dynamic> json) => Deposits(
        data: List<Deposit>.from(json["data"].map((x) => Deposit.fromJson(x))),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class Deposit {
  Deposit({
    required this.accountId,
    required this.issuerId,
    required this.amount,
    required this.regDate,
  });

  String accountId;
  String issuerId;
  double amount;
  double regDate;

  factory Deposit.fromJson(Map<String, dynamic> json) => Deposit(
        accountId: json["account_id"],
        issuerId: json["issuer_id"],
        amount: json["amount"],
        regDate: json["reg_date"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "account_id": accountId,
        "issuer_id": issuerId,
        "amount": amount,
        "reg_date": regDate,
      };
}
