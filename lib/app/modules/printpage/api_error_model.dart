// ignore: file_names

import 'dart:convert';

ApiError apiErrorFromJson(String str) => ApiError.fromJson(json.decode(str));

String apiErrorToJson(ApiError data) => json.encode(data.toJson());

class ApiError {
  ApiError({
    required this.msgs,
  });

  List<String> msgs;

  factory ApiError.fromJson(Map<String, dynamic> json) => ApiError(
        msgs: List<String>.from(json["msgs"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "msgs": List<dynamic>.from(msgs.map((x) => x)),
      };
}
