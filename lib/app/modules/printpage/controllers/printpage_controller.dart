// ignore_for_file: unnecessary_null_comparison, non_constant_identifier_names

import 'package:evd_mobile/app/data/api_helper.dart';
import 'package:evd_mobile/app/modules/batch/controllers/batch_controller.dart';
import 'package:evd_mobile/app/modules/bulkdetail/select_print_model.dart';
import 'package:evd_mobile/app/modules/distributor/controllers/accountholder_list_controller.dart';
import 'package:evd_mobile/app/modules/bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import 'package:evd_mobile/app/modules/printhistory/controllers/printhistory_controller.dart';
import 'package:evd_mobile/app/modules/printpage/api_error_model.dart';
import 'package:evd_mobile/app/modules/summaryreport/controllers/summaryreport_controller.dart';
import 'package:evd_mobile/app/modules/widgets/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import '../print_data_model.dart';
import '../vochure_model.dart';

class PrintpageController extends GetxController with StateMixin<List<Datum>> {
  final ApiHelper _apiHelper = Get.find();
  RxInt isSelected = 0.obs;
  RxInt quantity = 0.obs;
  final qcontroller = TextEditingController(text: '0');
  List arg = Get.arguments;
  int faceVallue = 0;
  RxInt totalQuantity = 0.obs;
  RxDouble totalPrice = 0.0.obs;
  RxList<Datum> data = <Datum>[].obs;
  bool hasData = false;
  RxList<Datum> list_voucher = <Datum>[].obs;
  RxList<Datum> reprintResponse = <Datum>[].obs;
  RxBool isblue = false.obs;
//reprint variables
  RxBool isAllChecked = false.obs;
  RxList checkedCounter = [].obs;

  RxBool isButtonActive = false.obs;
  RxBool isReprintButtonActive = false.obs;

  RxMap selectedVochureochureMap = {}.obs;
  BluetoothconnectionController bcontroller =
      Get.find<BluetoothconnectionController>();
  AccountholderController account = Get.put(AccountholderController());
  RxBool autovalidate = false.obs;
  @override
  void onInit() {
    super.onInit();
    qcontroller.addListener(getTotal);

    totalQuantity.value = arg[1];
    faceVallue = arg[0];
  }

  @override
  void onClose() {}

  @override
  void dispose() {
    qcontroller.dispose();
    super.dispose();
  }

  double getTotal() {
    if ((qcontroller.text == '') || (int.parse(qcontroller.text) <= 0)) {
      totalPrice.value = 0.0;
      isButtonActive.value = false;
    } else if (int.parse(qcontroller.text) >= 0) {
      totalPrice.value = faceVallue * int.parse(qcontroller.text) -
          (faceVallue *
              int.parse(qcontroller.text) *
              (account.ds.commission / 100));
      isButtonActive.value = true;
    }

    if (totalPrice.value <= 0) {
      totalPrice.value = 0.0;
    }
    return totalPrice.value;
  }

  RxInt counter = 0.obs;
  RxList<Datum> listofdatum = <Datum>[].obs;
  void getVouchers() {
    change(null, status: RxStatus.empty());

    _apiHelper.getVoucheWithFaceValue(faceVallue).futureValue(
      (dynamic value) {
        List<Datum> dt = voucherFromJson(value).data;
        for (var i = 0; i < dt.length; i++) {
          listofdatum.add(voucherFromJson(value).data[i]);
        }
        change(listofdatum, status: RxStatus.success());
      },
      retryFunction: getVouchers,
    );
  }

  final BluetoothconnectionController blue =
      Get.put(BluetoothconnectionController());
  printRequest() async {
    // Logger().d(account.ds.id.toString());
    list_bulk_voucher.clear();
    selectedVochureochureMap.clear();
    Logger().d('startting.....');
    Logger().d(
        'Amount entered is.....' + double.parse(qcontroller.text).toString());
    var body = Welcome(
        amount: double.parse(qcontroller.text),
        faceValue: faceVallue,
        remark: "remark");
    Logger().i(body.toJson());
    try {
      bool? isOn = await blue.bluetooth.isOn;
      bool? isConnected = await blue.bluetooth.isConnected;

      if (!isOn!) {
        isblue.value = false;
        blue.show('Turn on Bluetooth First'.tr);
      } else if (!isConnected!) {
        isblue.value = false;
        blue.show('No Bluetooth Connection'.tr);
      } else {
        isblue.value = true;

        _apiHelper.printRequest(body).then((value) {
          // Logger().d(value.body);
          // Logger().d(value.statusCode);
          if (value.statusCode == 200) {
            list_voucher.value = datuFromJson(value.body);
            bcontroller.startPrint(list_voucher, account.ds.agentUsername);
            reprintResponse.value = datuFromJson(value.body);
            for (var i = 0; i < reprintResponse.length; i++) {
              selectedVochureochureMap[reprintResponse[i].serialNo] = false;
              Logger().i(reprintResponse[i].serialNo);
              PrinthistoryController history =
                  Get.put(PrinthistoryController());
              BatchController batch = Get.put(BatchController());
              SummaryreportController summary =
                  Get.put(SummaryreportController());
              account.getAccountHolder();
              history.getVouchers(true);
              history.getPrintedVouchers();
              batch.getFaceValueCount(true);
              batch.getAccountBalance();
              summary.onReady();
            }
            qcontroller.clear();
            isButtonActive.value = false;
          } else {
            Logger().d(value.statusCode);
            var err = apiErrorFromJson(value.body);
            err.msgs[0] != null ? toast(err.msgs[0]) : toast('Forbiden!');
          }

          Logger().d(list_voucher.length);
        });
      }
    } catch (e) {
      Logger().i('hahahaha');
      Logger().d(e);
    }
  }

  var selectedId = [];
  List<SelectPrintVoucher> list_bulk_voucher = [];
  selectAndReprint() async {
    checkedCounter.value = [];
    selectedVochureochureMap.forEach((key, value) {
      key = false;
    });
    var selectedSerial = [];
    selectedId.clear();
    for (var element in selectedVochureochureMap.entries) {
      Logger().i(element.key);
      element.value ? selectedSerial.add(element.key) : null;
    }
    for (var element in reprintResponse) {
      Logger().i(element.serialNo);
      if ((selectedSerial.contains(element.serialNo) &&
          element.reprintCount < 3)) {
        Logger().i(element.id);
        selectedId.add(element.id);
      }
    }

    bool? isOn = await blue.bluetooth.isOn;
    bool? isConnected = await blue.bluetooth.isConnected;
    if (!isOn!) {
      blue.show('Turn on Bluetooth First');
    } else if (!isConnected!) {
      blue.show('No Bluetooth Connection');
    } else {
      Logger().d(selectedId);
      _apiHelper.selectReprint(selectedId).then((value) {
        if (value.statusCode == 200) {
          Logger().d(value.body);
          list_bulk_voucher = selectPrintVoucherFromJson(value.body);
          blue.startSelectRePrint(list_bulk_voucher, account.ds.agentUsername);
          for (var voucher in reprintResponse) {
            for (var element in list_bulk_voucher) {
              if (voucher.id == element.id) {
                voucher.reprintCount = element.reprintCount + 1;
                counter.value++;
              }
            }
          }
        } else {
          var err = apiErrorFromJson(value.body);
          err.msgs[0] != '' || err.msgs[0] != null
              ? toast(err.msgs[0])
              : toast('Internal Server Error!');
        }
      });
    }
  }
}
