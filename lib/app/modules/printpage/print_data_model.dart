// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

Welcome welcomeFromJson(String str) => Welcome.fromJson(json.decode(str));

String welcomeToJson(Welcome data) => json.encode(data.toJson());

class Welcome {
    Welcome({
        this.amount,
        this.faceValue,
        this.remark,
    });

    double? amount;
    int? faceValue;
    String? remark;

    factory Welcome.fromJson(Map<String, dynamic> json) => Welcome(
        amount: json["amount"],
        faceValue: json["face_value"],
        remark: json["remark"],
    );

    Map<String, dynamic> toJson() => {
        "amount": amount,
        "face_value": faceValue,
        "remark": remark,
    };
}
