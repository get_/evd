// To parse this JSON data, do
//
//     final printedVouchers = printedVouchersFromJson(jsonString);

import 'dart:convert';

PrintedVouchers printedVouchersFromJson(String str) =>
    PrintedVouchers.fromJson(json.decode(str));

String printedVouchersToJson(PrintedVouchers data) =>
    json.encode(data.toJson());

class PrintedVouchers {
  PrintedVouchers({
    required this.data,
    required this.total,
  });

  List<PrintedVoucher> data;
  int total;

  factory PrintedVouchers.fromJson(Map<String, dynamic> json) =>
      PrintedVouchers(
        data: List<PrintedVoucher>.from(
            json["data"].map((x) => PrintedVoucher.fromJson(x))),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class PrintedVoucher {
  PrintedVoucher({
    this.id,
    required this.accountId,
    required this.amount,
    required this.faceValue,
    required this.regDate,
    required this.remark,
  });

  String? id;
  String accountId;
  double amount;
  int faceValue;
  int regDate;
  String remark;

  factory PrintedVoucher.fromJson(Map<String, dynamic> json) => PrintedVoucher(
        id: json["id"],
        accountId: json["account_id"],
        amount: json["amount"],
        faceValue: json["face_value"],
        regDate: json["reg_date"],
        remark: json["remark"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "account_id": accountId,
        "amount": amount,
        "face_value": faceValue,
        "reg_date": regDate,
        "remark": remark,
      };
}
