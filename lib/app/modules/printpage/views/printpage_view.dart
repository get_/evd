// ignore_for_file: invalid_use_of_protected_member, curly_braces_in_flow_control_structures, unrelated_type_equality_checks, unnecessary_string_interpolations, must_be_immutable

import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:evd_mobile/app/modules/bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import 'package:evd_mobile/app/modules/home/controllers/home_controller.dart';
import 'package:logger/logger.dart';
import '../controllers/printpage_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PrintpageView extends GetView<PrintpageController> {
  var homeController = HomeController();
  var bController = Get.find<BluetoothconnectionController>();

  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  var autovalidate = AutovalidateMode.disabled;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.primaryColor,
          title: Text('Print card'.tr),
          elevation: 0,
          actions: [
            IconButton(
              onPressed: () {
                builBottomModalSheet(context);
              },
              icon: Icon(
                Icons.bluetooth,
                color: AppColors.whiteColor,
                size: Dimens.fontSize24,
              ),
            )
          ],
        ),
        body: Obx(() => controller.totalQuantity.value <= 0
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(image: AssetImage(AppImages.nodata)),
                    RichText(
                      text: TextSpan(
                          text: 'No cards with '.tr,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          children: <TextSpan>[
                            TextSpan(
                              text: '${controller.faceVallue}',
                              style: TextStyle(
                                fontSize: 24,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            TextSpan(
                              text: ' Face value.'.tr,
                              style:
                                  TextStyle(fontSize: 18, color: Colors.black),
                            ),
                          ]),
                    ),
                  ],
                ),
              )
            : SingleChildScrollView(
                primary: false,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          '${controller.faceVallue} ${'BIRR'.tr} ${'Cards'.tr} ',
                          style: TextStyle(
                              color: AppColors.secondaryColor,
                              fontSize: Dimens.fontSize24,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(),
                      Container(
                        height: 100,
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text('Quantity'.tr),
                                  ),
                                  Expanded(
                                    child: Form(
                                        key: _key,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextFormField(
                                            onFieldSubmitted: (val) {
                                              if (val.isEmpty || val == '') {
                                                controller.qcontroller.text =
                                                    '0';
                                              }
                                            },
                                            textAlign: TextAlign.center,
                                            autovalidateMode: autovalidate,
                                            autofocus: false,
                                            controller: controller.qcontroller,
                                            keyboardType: TextInputType.number,
                                          ),
                                        )),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: VerticalDivider(
                                color: Colors.black,
                                width: 20,
                              ),
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text('Total'.tr),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 18.0,
                                      left: 8,
                                    ),
                                    child: Obx(() => Text(
                                          controller.totalPrice.value
                                                  .toString() +
                                              ' Birr'.tr,
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold),
                                        )),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Obx(() => controller.isButtonActive.value
                          ? Container(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    onSurface: AppColors.secondaryColor),
                                onPressed: () {
                                  if (_key.currentState!.validate()) {
                                    // bool isPrint = true;
                                    // if (isPrint) {
                                    controller.printRequest();
                                    controller.isReprintButtonActive.value =
                                        true;
                                    // } else {
                                    //   controller.selectAndReprint();
                                    // }
                                    // Get.back();
                                  }
                                },
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                    colors: [
                                      AppColors.secondaryColor,
                                      Colors.redAccent
                                    ],
                                  )),
                                  padding: const EdgeInsets.all(15.0),
                                  child: Text(
                                    "Print".tr.toUpperCase(),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            )
                          : Container(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    onSurface: AppColors.white),
                                onPressed: null,
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  decoration: BoxDecoration(
                                    color: Colors.red.shade100,
                                  ),
                                  padding: const EdgeInsets.all(15.0),
                                  child: Text(
                                    "Print".tr.toUpperCase(),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            )),
                      Divider(),
                      Obx(
                        () => (!controller.isReprintButtonActive.value &&
                                    controller.reprintResponse.isEmpty) ||
                                !controller.isblue.value
                            // ||
                            // !controller.isblue.value
                            ? Text('')
                            : Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Column(
                                  children: [
                                    Center(
                                      child: Text(
                                        'Print Transaction'.tr,
                                        style: TextStyle(
                                            color: AppColors.primaryColor,
                                            fontSize: Dimens.fontSize18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Divider(height: 6, color: Colors.grey),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Obx(() => controller
                                                .checkedCounter.isEmpty
                                            ? Material(
                                                color: Colors.grey,
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: InkWell(
                                                  onTap: null,
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: Container(
                                                    width: 100,
                                                    height: 30,
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15),
                                                    ),
                                                    alignment: Alignment.center,
                                                    child: Text('Reprint'
                                                        .tr
                                                        .toUpperCase()),
                                                  ),
                                                ),
                                              )
                                            : Material(
                                                color: AppColors.primaryColor,
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: InkWell(
                                                  onTap: () {
                                                    controller
                                                        .selectAndReprint();
                                                  },
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: Container(
                                                    width: 100,
                                                    height: 30,
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15),
                                                    ),
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      'Reprint'
                                                          .tr
                                                          .toUpperCase(),
                                                      style: TextStyle(
                                                          color: AppColors
                                                              .whiteColor),
                                                    ),
                                                  ),
                                                ),
                                              ))
                                      ],
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 12),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Obx(() {
                                            return Flexible(
                                              child: Text(
                                                  '${controller.checkedCounter.length} ' +
                                                      'selected'.tr +
                                                      ' ${controller.list_voucher.length} ' +
                                                      'Cards'.tr),
                                            );
                                          }),
                                          Row(
                                            children: [
                                              Text('Select All'.tr),
                                              Obx(() => Checkbox(
                                                  activeColor:
                                                      AppColors.secondaryColor,
                                                  value: controller
                                                      .isAllChecked.value,
                                                  onChanged: (newvalue) {
                                                    Logger().d(newvalue);
                                                    controller.isAllChecked
                                                        .value = newvalue!;

                                                    controller
                                                        .selectedVochureochureMap
                                                        .forEach((key, value) {
                                                      controller
                                                              .selectedVochureochureMap[
                                                          key] = newvalue;
                                                    });
                                                    var validSerials = controller
                                                        .reprintResponse.value
                                                        .where((element) =>
                                                            element
                                                                .reprintCount <
                                                            3)
                                                        .map((e) => e.serialNo)
                                                        .toSet();
                                                    if (newvalue == true) {
                                                      controller.checkedCounter
                                                          .clear();
                                                      controller.checkedCounter
                                                          .addAll(
                                                        controller
                                                            .selectedVochureochureMap
                                                            .keys
                                                            .where(
                                                          (element) =>
                                                              validSerials
                                                                  .contains(
                                                            element,
                                                          ),
                                                        ),
                                                      );
                                                    } else if (newvalue ==
                                                        false) {
                                                      controller.checkedCounter
                                                          .clear();
                                                    }
                                                  })),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Obx(() {
                                      return controller
                                              .selectedVochureochureMap.isEmpty
                                          ? Container(
                                              child: Center(
                                                  child:
                                                      CircularProgressIndicator()))
                                          : ListView.builder(
                                              physics: ScrollPhysics(),
                                              primary: true,
                                              shrinkWrap: true,
                                              // scrollDirection: Axis.vertical,
                                              itemCount: controller
                                                  .selectedVochureochureMap
                                                  .length,
                                              itemBuilder: (context, index) {
                                                return Obx(
                                                  () => CheckboxListTile(
                                                    tileColor: controller
                                                                .reprintResponse
                                                                .value[index]
                                                                .reprintCount ==
                                                            3
                                                        ? Colors.grey.shade100
                                                        : null,
                                                    value: controller
                                                                .reprintResponse
                                                                .value[index]
                                                                .reprintCount ==
                                                            3
                                                        ? true
                                                        : controller
                                                                .selectedVochureochureMap[
                                                            controller
                                                                .reprintResponse
                                                                .value[index]
                                                                .serialNo],
                                                    activeColor: controller
                                                                .reprintResponse
                                                                .value[index]
                                                                .reprintCount ==
                                                            3
                                                        ? Colors.grey.shade400
                                                        : AppColors
                                                            .secondaryColor,
                                                    title: Text('SN:  ' +
                                                        controller
                                                            .reprintResponse
                                                            .value[index]
                                                            .serialNo),
                                                    subtitle: Obx(() => Text(
                                                        'COUNT:  ' +
                                                            controller
                                                                .reprintResponse
                                                                .value[index]
                                                                .reprintCount
                                                                .toString())),
                                                    onChanged: (newValue) {
                                                      controller
                                                                  .reprintResponse
                                                                  .value[index]
                                                                  .reprintCount ==
                                                              3
                                                          ? newValue = null
                                                          : newValue;
                                                      controller.selectedVochureochureMap[
                                                              controller
                                                                  .reprintResponse
                                                                  .value[index]
                                                                  .serialNo] =
                                                          newValue;

                                                      if (newValue == false) {
                                                        controller.isAllChecked
                                                            .value = false;
                                                        if (controller
                                                            .checkedCounter
                                                            .isNotEmpty) {
                                                          controller
                                                              .checkedCounter
                                                              .removeAt(controller
                                                                      .checkedCounter
                                                                      .length -
                                                                  1);
                                                        }
                                                      } else if (newValue ==
                                                          true) {
                                                        controller
                                                            .checkedCounter
                                                            .add(true);
                                                      }
                                                      if (!controller
                                                          .selectedVochureochureMap
                                                          .containsValue(
                                                              false)) {
                                                        controller.isAllChecked
                                                            .value = true;
                                                      }
                                                    },
                                                  ),
                                                );
                                              });
                                    }),
                                    SizedBox(height: 10),
                                    Divider(),
                                    SizedBox(height: 10),
                                  ],
                                ),
                              ),
                      )
                    ],
                  ),
                ),
              )));
  }

  Future<void> builBottomModalSheet(BuildContext context) {
    return showModalBottomSheet<void>(
        context: context,
        isDismissible: true,
        builder: (BuildContext context) {
          return Container(
              height: 250,
              color: AppColors.whiteColor,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: ListView(
                  padding: EdgeInsets.all(10),
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Device'.tr + ': ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Obx(() => bController.devices.isEmpty
                            ? Center(child: Text('No Device'.tr))
                            : DropdownButton<BluetoothDevice>(
                                hint: Text('select device'.tr),
                                focusColor: Colors.white,
                                value: bController.device.value.name == ''
                                    ? null
                                    : bController.device.value,
                                style: TextStyle(color: Colors.white),
                                iconEnabledColor: Colors.black,
                                items: bController.devices
                                    .map<DropdownMenuItem<BluetoothDevice>>(
                                        (product) {
                                  return DropdownMenuItem<BluetoothDevice>(
                                    value: product,
                                    child: Text(
                                      product.name!,
                                      style: TextStyle(color: Colors.orange),
                                    ),
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  bController.device.value = value!;
                                },
                              )),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: AppColors.primaryColor),
                            onPressed: () {
                              bController.initPlatformState();
                            },
                            child: Text(
                              'Refresh'.tr,
                              style: TextStyle(color: AppColors.whiteColor),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Obx(() => Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: bController.connected.value
                                        ? Colors.red
                                        : Colors.green),
                                onPressed: bController.connected.value
                                    ? bController.disconnect
                                    : bController.connect,
                                child: bController.connecting.value
                                    ? Container(
                                        width: 100,
                                        height: 20,
                                        child: Row(
                                          children: [
                                            CircularProgressIndicator(
                                              color: AppColors.whiteColor,
                                            ),
                                            Expanded(
                                              child: Text(
                                                'loading...',
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color:
                                                        AppColors.whiteColor),
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    : Text(
                                        bController.connected.value
                                            ? 'Disconnect'.tr
                                            : 'Connect'.tr,
                                        style: TextStyle(color: Colors.white),
                                      ),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ));
        });
  }

  // contentBox(context, bool isPrint) {
  //   return Stack(
  //     children: <Widget>[
  //       Container(
  //         padding:
  //             EdgeInsets.only(left: 20, top: 45 + 20, right: 20, bottom: 20),
  //         margin: EdgeInsets.only(top: 45),
  //         decoration: BoxDecoration(
  //             shape: BoxShape.rectangle,
  //             color: Colors.white,
  //             borderRadius: BorderRadius.circular(20),
  //             boxShadow: [
  //               BoxShadow(
  //                   color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
  //             ]),
  //         child: Column(
  //           mainAxisSize: MainAxisSize.min,
  //           children: <Widget>[
  //             Text(
  //               'Confirm print'.tr,
  //               style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
  //             ),
  //             SizedBox(
  //               height: 15,
  //             ),
  //             Flexible(
  //               child: Text(
  //                 isPrint
  //                     ? 'Are you sure you want to print'.tr +
  //                         controller.qcontroller.text +
  //                         ' cards of ${controller.faceVallue} birr?'
  //                     : 'Reprint'.tr + '?',
  //                 style: TextStyle(fontSize: 14),
  //                 textAlign: TextAlign.center,
  //               ),
  //             ),
  //             SizedBox(
  //               height: 22,
  //             ),
  //             Row(
  //               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //               children: [
  //                 Align(
  //                   alignment: Alignment.bottomLeft,
  //                   child: TextButton(
  //                       onPressed: () {
  //                         if (isPrint) {
  //                           controller.printRequest();
  //                           controller.isReprintButtonActive.value = true;
  //                         } else {
  //                           controller.selectAndReprint();
  //                         }
  //                         Get.back();
  //                       },
  //                       child: Text(
  //                         'Confirm'.tr,
  //                         style: TextStyle(
  //                             fontSize: Dimens.fontSize18,
  //                             color: AppColors.primaryColor),
  //                       )),
  //                 ),
  //                 Align(
  //                   alignment: Alignment.bottomRight,
  //                   child: TextButton(
  //                       onPressed: () {
  //                         Get.back();
  //                       },
  //                       child: Text(
  //                         'Cancel'.tr,
  //                         style: TextStyle(
  //                             fontSize: Dimens.fontSize18,
  //                             color: AppColors.primaryColor),
  //                       )),
  //                 ),
  //               ],
  //             ),
  //           ],
  //         ),
  //       ),
  //       Positioned(
  //         left: 20,
  //         right: 20,
  //         child: CircleAvatar(
  //             backgroundColor: AppColors.secondaryColor,
  //             radius: 45,
  //             child: ClipRRect(
  //                 borderRadius: BorderRadius.all(Radius.circular(45)),
  //                 child: Icon(
  //                   Icons.print,
  //                   size: MediaQuery.of(context).size.width * 0.17,
  //                   color: AppColors.whiteColor,
  //                 ))),
  //       ),
  //     ],
  //   );
  // }

}
