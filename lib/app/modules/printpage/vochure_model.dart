// To parse this JSON data, do
//
//     final voucher = voucherFromJson(jsonString);

import 'dart:convert';

Voucher voucherFromJson(String str) => Voucher.fromJson(json.decode(str));
List<Datum> datuFromJson(String str) =>
    List<Datum>.from(json.decode(str).map((x) => Datum.fromJson(x)));

String voucherToJson(Voucher data) => json.encode(data.toJson());

class Voucher {
  Voucher({
    required this.data,
    required this.total,
  });

  List<Datum> data;
  int total;

  factory Voucher.fromJson(Map<String, dynamic> json) => Voucher(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class Datum {
  Datum({
    required this.id,
    required this.batchId,
    required this.batchNo,
    required this.serialNo,
    required this.voucherNumber,
    required this.faceValue,
    required this.isPrint,
    required this.reprintCount,
    required this.operatorId,
    required this.regDate,
  });

  String id;
  String batchId;
  String batchNo;
  String serialNo;
  String voucherNumber;
  int faceValue;
  bool isPrint;
  int reprintCount;
  String operatorId;
  int regDate;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        batchId: json["batch_id"],
        batchNo: json["batch_no"],
        serialNo: json["serial_no"],
        voucherNumber: json["voucher_number"],
        faceValue: json["face_value"],
        isPrint: json["is_print"],
        reprintCount: json["reprint_count"],
        operatorId: json["operator_id"],
        regDate: json["reg_date"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "batch_id": batchId,
        "batch_no": batchNo,
        "serial_no": serialNo,
        "voucher_number": voucherNumber,
        "face_value": faceValue,
        "is_print": isPrint,
        "reprint_count": reprintCount,
        "operator_id": operatorId,
        "reg_date": regDate,
      };
}
