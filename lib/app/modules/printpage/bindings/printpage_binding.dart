import 'package:get/get.dart';

import '../controllers/printpage_controller.dart';

class PrintpageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PrintpageController>(
      () => PrintpageController(),
    );
  }
}
