import 'dart:io';
import 'package:evd_mobile/app/modules/bluetoothconnection/controllers/bluetoothconnection_controller.dart';
import 'package:get/get.dart';

import '../../../common/storage/storage.dart';
import '../../../common/util/localization.dart';
import '../../../routes/app_pages.dart';

class HomeController extends GetxController {
  RxString chosenValue = 'English'.obs;
  var langCtrl = Get.put(LocaleString());
  RxInt index = 0.obs;
  updateLanguage() {
    Storage.storage.write('lang', chosenValue.value);
    print("''''''''''''''''''''''''''''''''" + Storage.storage.read('lang'));
    update();
  }

  final RxList _dataList = RxList();
  late File selectedFile;

  List<dynamic> get dataList {
    return _dataList;
  }

  set dataList(List<dynamic> dataList) {
    _dataList.addAll(dataList);
  }

  void onEditProfileClick() {
    Get.back();
  }

  void onFaqsClick() {
    Get.back();
  }

  void onLogoutClick() {
    
    Get.offAllNamed(Routes.LOGIN);
    Storage.clearStorage();
    BluetoothconnectionController().disconnect();
    //Specify the INITIAL SCREEN you want to display to the user after logout
  }
}
