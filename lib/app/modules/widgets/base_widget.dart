import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:evd_mobile/app/data/interface_controller/api_interface_controller.dart';
import 'package:evd_mobile/app/modules/widgets/custom_retry_widget.dart';

export 'package:evd_mobile/app/common/util/exports.dart';

class BaseWidget extends GetView<ApiInterfaceController> {
  ///A widget with only custom retry button widget.
  final Widget child;

  BaseWidget({
    Key? key,
    required this.child,
  }) : super(key: key);
  @override
  var controller = Get.put(ApiInterfaceController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Stack(
        children: [
          Positioned.fill(
            child: child,
          ),
          Visibility(
            visible: controller.retry != null && controller.error != null,
            child: Positioned.fill(
              child: Scaffold(
                body: CustomRetryWidget(
                  onPressed: () {
                    controller.error = null;
                    controller.retry!.call();
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
