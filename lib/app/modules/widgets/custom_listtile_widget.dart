import 'package:flutter/material.dart';
import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class CustomListTileWidget extends StatelessWidget {
  final Color titlecolor;
  final String title;
  final Function() onTap;
  final Widget? leading;

  const CustomListTileWidget({
    Key? key,
    required this.title,
    required this.onTap,
    this.leading, required this.titlecolor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      tileColor: titlecolor,
      onTap: onTap,
      title: Text(
        title.tr,
        style: AppTextStyle.semiBoldStyle.copyWith(
          color: AppColors.black,
          fontSize: Dimens.fontSize16,
        ),
      ),
      leading: leading ?? const Icon(Icons.arrow_forward_ios_sharp),
    );
  }
}
