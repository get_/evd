import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'dart:math' as math;

import 'base_widget.dart';

PreferredSize CustomHeader(context) {
  return PreferredSize(
      child: Container(
        width: MediaQuery.of(context).size.width * 1,
        color: AppColors.primaryColor,
        child: Padding(
          padding: const EdgeInsets.only(top: 20, left: 10),
          child: AppBar(
            elevation: 0,
            backgroundColor: AppColors.primaryColor,
            iconTheme: IconThemeData(color: AppColors.secondaryColor),
            actions: [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 30, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.grey,
                      radius: 25.0,
                      child: ClipRRect(
                        child: Image.asset(AppImages.profile),
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text('Admin Name'),
                            Transform.rotate(
                                angle: 90 * math.pi / 180,
                                child: Icon(
                                  Icons.play_arrow_rounded,
                                  color: AppColors.whiteColor,
                                ))
                          ],
                        ),
                        Text('Profile'.tr)
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      preferredSize: Size.fromHeight(100));
}
