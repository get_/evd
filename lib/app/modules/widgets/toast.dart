import 'package:evd_mobile/app/common/values/app_colors.dart';
import 'package:fluttertoast/fluttertoast.dart';

Future<bool?> toast(String msg) {
  return Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: AppColors.secondaryColor,
      textColor: AppColors.white,
      fontSize: 16.0);
}
