import 'package:evd_mobile/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:evd_mobile/app/modules/home/controllers/home_controller.dart';
import 'package:evd_mobile/app/modules/widgets/custom_drawer_header.dart';
import 'package:evd_mobile/app/modules/widgets/custom_listtile_widget.dart';
import 'package:get/get.dart';

class CustomDrawerWidget extends StatelessWidget {
  final HomeController controller;

  CustomDrawerWidget({
    Key? key,
    required this.controller,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.7,
        child: Drawer(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomDrawerHeader(
                homeController: controller,
              ),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: [
                    CustomListTileWidget(
                      leading: Icon(Icons.home),
                      title: Strings.home.tr,
                      onTap: () {
                        Get.currentRoute == Routes.BATCH
                            ? Get.back()
                            : Get.toNamed(Routes.BATCH);
                      },
                      titlecolor: Get.currentRoute == Routes.BATCH
                          ? Colors.orange.withOpacity(0.5)
                          : Colors.white24,
                    ),
                    CustomListTileWidget(
                      titlecolor: Get.currentRoute == Routes.SUMMARYREPORT
                          ? Colors.orange.withOpacity(0.5)
                          : Colors.white24,
                      leading: Icon(Icons.summarize),
                      title: Strings.history.tr,
                      onTap: () {
                        Get.currentRoute == Routes.SUMMARYREPORT
                            ? Get.back()
                            : Get.toNamed(Routes.SUMMARYREPORT);
                      },
                    ),
                    CustomListTileWidget(
                      titlecolor: Get.currentRoute == Routes.PRINTHISTORY
                          ? Colors.orange.withOpacity(0.5)
                          : Colors.white24,
                      leading: Icon(Icons.history),
                      title: Strings.printHistory.tr,
                      onTap: () {
                        Get.currentRoute == Routes.PRINTHISTORY
                            ? Get.back()
                            : Get.toNamed(Routes.PRINTHISTORY);
                      },
                    ),
                    CustomListTileWidget(
                      titlecolor: Get.currentRoute == Routes.DEPOSIT
                          ? Colors.orange.withOpacity(0.5)
                          : Colors.white24,
                      leading: Icon(Icons.account_balance),
                      title: Strings.deposit.tr,
                      onTap: () {
                        Get.currentRoute == Routes.DEPOSIT
                            ? Get.back()
                            : Get.toNamed(Routes.DEPOSIT);
                      },
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        title: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            elevation: 0,
                            focusColor: AppColors.primaryColor,
                            value: controller.chosenValue.value,
                            iconEnabledColor: AppColors.primaryColor,
                            items: <String>[
                              'English',
                              'አማርኛ',
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Center(
                                  child: Text(
                                    value,
                                    style: TextStyle(
                                        fontSize: 17,
                                        color: AppColors.primaryColor),
                                  ),
                                ),
                              );
                            }).toList(),
                            onChanged: (value) {
                              controller.chosenValue.value = value!;
                              controller.updateLanguage();
                              if (controller.chosenValue.value == "English") {
                                // Storage.storage.write('lang', 'English');
                                controller.langCtrl.changeLocale('English');
                              } else if (controller.chosenValue.value ==
                                  "አማርኛ") {
                                // Storage.storage.write('lang', 'አማርኛ');
                                controller.langCtrl.changeLocale('አማርኛ');
                              }
                            },
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              CustomListTileWidget(
                  titlecolor: Colors.orange.withOpacity(0.04),
                  leading: Icon(Icons.logout_rounded),
                  title: Strings.logOut,
                  onTap: () => showDialog(
                      context: context,
                      builder: (context) => Dialog(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            elevation: 0,
                            backgroundColor: Colors.transparent,
                            child: contentBox(context),
                          ))),
            ],
          ),
        ),
      ),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
          // margin: EdgeInsets.only(top: 5),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image(
                image: AssetImage(AppImages.logout),
                height: MediaQuery.of(context).size.height * 0.1,
                // width: MediaQuery.of(context).size.width * 0.1,
              ),
              Text(
                'Confirm Logout'.tr,
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 15,
              ),
              Flexible(
                child: Text(
                  'Are you sure you want to logout?'.tr,
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 22,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Align(
                    alignment: Alignment.bottomRight,
                    child: TextButton(
                        onPressed: () {
                          controller.onLogoutClick();
                        },
                        child: Text(
                          'Confirm'.tr,
                          style: TextStyle(
                              fontSize: Dimens.fontSize18,
                              color: AppColors.primaryColor),
                        )),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: TextButton(
                        onPressed: () {
                          Get.back();
                        },
                        child: Text(
                          'Cancel'.tr,
                          style: TextStyle(
                              fontSize: Dimens.fontSize18,
                              color: AppColors.primaryColor),
                        )),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
