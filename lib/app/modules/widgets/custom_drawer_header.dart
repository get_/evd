import 'package:evd_mobile/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/util/exports.dart';
import '../distributor/controllers/accountholder_list_controller.dart';
import '../home/controllers/home_controller.dart';

class CustomDrawerHeader extends StatelessWidget {
  final HomeController homeController;

  const CustomDrawerHeader({
    Key? key,
    required this.homeController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var con = Get.put(AccountholderController());
    return Container(
      color: AppColors.primaryColor,
      height: 160.w,
      // padding: EdgeInsets.fromLTRB(15.w, 15.w, 15.w, 7.w),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
              alignment: Alignment.topRight,
              child: IconButton(
                  onPressed: () {
                    Get.currentRoute == Routes.LIST_ACCOUNTHOLDER
                        ? Get.back()
                        : Get.toNamed(Routes.LIST_ACCOUNTHOLDER);
                  },
                  icon: Icon(
                    Icons.edit,
                    color: AppColors.secondaryColor,
                  ))),
          InkWell(
            onTap: () {
              Get.currentRoute == Routes.LIST_ACCOUNTHOLDER
                  ? Get.back()
                  : Get.toNamed(Routes.LIST_ACCOUNTHOLDER);
            },
            child: Center(
              child: CircleAvatar(
                radius: 40,
                backgroundColor: AppColors.secondaryColor,
                child: 
                // con.ds.fullName == ''
                    // ? Text('')
                    // : 
                    Text(
                        'Profile'.substring(0, 1).toUpperCase(),
                        style: TextStyle(
                            color: AppColors.whiteColor,
                            fontSize: 60,
                            fontWeight: FontWeight.bold),
                      ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.01),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  con.ds.fullName,
                  style: AppTextStyle.semiBoldStyle.copyWith(
                      fontSize: Dimens.fontSize16, color: AppColors.whiteColor),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
