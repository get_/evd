import 'package:flutter/material.dart';
import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

import 'custom_text_button.dart';

class CustomRetryWidget extends StatelessWidget {
  final String error;
  final VoidCallback onPressed;

  CustomRetryWidget({
    Key? key,
    required this.onPressed,
    this.error = Strings.somethingWentWrong,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.whiteColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        // mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,

        children: [
          Center(
            child: Container(
                margin: EdgeInsets.only(bottom: 0, left: 0),
                height: MediaQuery.of(context).size.height * 0.35,
                child: Image(
                  image: AssetImage(AppImages.nowifi),
                  height: MediaQuery.of(context).size.height * 0.6,
                  width: MediaQuery.of(context).size.width * 0.6,
                )),
          ),
          Text('Ooops!',
              style: TextStyle(
                fontSize: 60,
              )),
          Text(error.tr),
          SizedBox(height: 16.h),
          CustomTextButton(
            buttonWidth: 105.w,
            height: 45,
            onPressed: onPressed,
            title: Strings.retry,
          ),
        ],
      ),
    );
  }
}
