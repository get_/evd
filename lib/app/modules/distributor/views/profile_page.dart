import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:evd_mobile/app/modules/distributor/controllers/accountholder_list_controller.dart';
import 'package:evd_mobile/app/modules/distributor/views/change_pwd_page.dart';
import 'package:evd_mobile/app/modules/home/controllers/home_controller.dart';
import 'package:evd_mobile/app/modules/widgets/custom_drawer_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class profilepage extends GetView<AccountholderController> {
  profilepage({Key? key}) : super(key: key);
  final controller = Get.put(AccountholderController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text('Profile'.tr),
          backgroundColor: AppColors.primaryColor,
        ),
        drawer: CustomDrawerWidget(controller: HomeController()),
        resizeToAvoidBottomInset: false,
        body: controller.obx(
          (state) => SingleChildScrollView(
            child: Stack(
              children: [
                Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                AppColors.primaryColor,
                                Colors.blue.shade600
                              ])),
                          child: Container(
                            width: double.infinity,
                            height: 280.0,
                            child: Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  CircleAvatar(
                                    radius: 50,
                                    backgroundColor: AppColors.secondaryColor,
                                    child: Text(
                                      state!.fullName
                                          .substring(0, 1)
                                          .toUpperCase(),
                                      style: TextStyle(
                                          color: AppColors.whiteColor,
                                          fontSize: 60,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Flexible(
                                    child: Text(
                                      state.fullName,
                                      style: TextStyle(
                                        fontSize: 22.0,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.17,
                                    child: Card(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 20.0, vertical: 5.0),
                                      clipBehavior: Clip.antiAlias,
                                      color: Colors.white,
                                      elevation: 5.0,
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0, vertical: 22.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    .12,
                                                child: Column(
                                                  children: <Widget>[
                                                    Flexible(
                                                      child: Text(
                                                        "Username".tr,
                                                        style: TextStyle(
                                                          color: AppColors
                                                              .primaryColor,
                                                          fontSize: 22.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                                    Flexible(
                                                      child: Text(
                                                        state.username,
                                                        style: TextStyle(
                                                          fontSize: 20.0,
                                                          color: AppColors
                                                              .secondaryColor,
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 5.0,
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    .12,
                                                child: Column(
                                                  children: <Widget>[
                                                    Flexible(
                                                      child: Text(
                                                        "Balance".tr,
                                                        style: TextStyle(
                                                          color: AppColors
                                                              .primaryColor,
                                                          fontSize: 22.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 5.0,
                                                    ),
                                                    Flexible(
                                                      child: Text(
                                                        Utils.numberFormat(
                                                            state.balance),
                                                        style: TextStyle(
                                                          fontSize: 20.0,
                                                          color: AppColors
                                                              .secondaryColor,
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    .12,
                                                child: Column(
                                                  children: <Widget>[
                                                    Text(
                                                      "Agent".tr,
                                                      style: TextStyle(
                                                        color: AppColors
                                                            .primaryColor,
                                                        fontSize: 22.0,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 5.0,
                                                    ),
                                                    Flexible(
                                                      child: Text(
                                                        state.agentUsername,
                                                        style: TextStyle(
                                                            fontSize: 20.0,
                                                            color: AppColors
                                                                .secondaryColor),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 30.0, horizontal: 16.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Comission(%)'.tr),
                                  Text(state.commission.toString())
                                ],
                              ),
                            ),
                            Divider(),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Telephone'.tr),
                                  Text(state.telephone)
                                ],
                              ),
                            ),
                            Divider(),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [Text('Email'.tr), Text(state.email)],
                              ),
                            ),
                            Divider(),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Address'.tr),
                                  Text(state.address)
                                ],
                              ),
                            ),
                            Divider(),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Company Name'.tr),
                                  Text(state.companyName)
                                ],
                              ),
                            ),
                            Divider(),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Account Type'.tr),
                                  Text('Distributor'.tr)
                                ],
                              ),
                            ),
                            Divider(),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 5),
                              child: Container(
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      onSurface: AppColors.secondaryColor),
                                  onPressed: () {
                                    Get.to(() => changepasswordpage());
                                  },
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.7,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                      colors: [
                                        AppColors.secondaryColor,
                                        Colors.redAccent
                                      ],
                                    )),
                                    padding: const EdgeInsets.all(15.0),
                                    child: Text(
                                      "Change Password".tr,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
