import 'package:evd_mobile/app/common/util/exports.dart';
import 'package:evd_mobile/app/common/util/validators.dart';
import 'package:evd_mobile/app/modules/distributor/controllers/accountholder_list_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class changepasswordpage extends StatelessWidget {
  changepasswordpage({Key? key}) : super(key: key);
  var controller = Get.put(AccountholderController());

  final _formKey = GlobalKey<FormState>();
  var autovalidate = AutovalidateMode.disabled;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Change Password'.tr),
        // centerTitle: true,
        backgroundColor: AppColors.primaryColor,
      ),
      resizeToAvoidBottomInset: false,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(38.0),
          child: Form(
            key: _formKey,
            autovalidateMode: autovalidate,
            child: ListView(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              children: [
                Image(
                  image: AssetImage(AppImages.pass),
                  height: 150,
                  width: 150,
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  'Current Password'.tr,
                  style: TextStyle(
                      fontSize: Dimens.fontSize16,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
                Obx(() => TextFormField(
                      enableInteractiveSelection: true,
                      keyboardType: TextInputType.text,
                      controller: controller.userOldPasswordController,
                      obscureText: controller.currentpasswordVisible.value,
                      validator: (value) {
                        return Validators.validatePassword(value);
                      },
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                          icon: Icon(
                            // Based on passwordVisible state choose the icon
                            controller.currentpasswordVisible.value
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: controller.currentpasswordVisible.value
                                ? AppColors.primaryColor
                                : AppColors.secondaryColor,
                          ),
                          onPressed: () {
                            // Update the state i.e. toogle the state of passwordVisible variable
                            controller.currentpasswordVisible.value =
                                !controller.currentpasswordVisible.value;
                          },
                        ),
                        hintText: 'Enter Current password'.tr,
                        hintStyle: TextStyle(
                          fontSize: Dimens.fontSize12,
                          color: Colors.grey,
                        ),
                      ),
                    )),
                Text(
                  'New Password'.tr,
                  style: TextStyle(
                      fontSize: Dimens.fontSize16,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
                Obx(() => TextFormField(
                      enableInteractiveSelection: true,
                      keyboardType: TextInputType.text,
                      controller: controller.userNewPasswordController,
                      obscureText: controller.newpasswordVisible.value,
                      validator: (value) {
                        return Validators.validatePassword(value);
                      },
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                          icon: Icon(
                            // Based on passwordVisible state choose the icon
                            controller.newpasswordVisible.value
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: controller.newpasswordVisible.value
                                ? AppColors.primaryColor
                                : AppColors.secondaryColor,
                          ),
                          onPressed: () {
                            // Update the state i.e. toogle the state of passwordVisible variable
                            controller.newpasswordVisible.value =
                                !controller.newpasswordVisible.value;
                          },
                        ),
                        hintText: 'Enter New password'.tr,
                        hintStyle: TextStyle(
                          fontSize: Dimens.fontSize12,
                          color: Colors.grey,
                        ),
                      ),
                    )),
                Text(
                  'Confirm Password'.tr,
                  style: TextStyle(
                      fontSize: Dimens.fontSize16,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
                Obx(() => TextFormField(
                      enableInteractiveSelection: true,
                      keyboardType: TextInputType.text,
                      controller: controller.userRepeatPasswordController,
                      obscureText: controller.reapeatpasswordVisible.value,
                      validator: (value) {
                        return Validators.validateConfirmPassword(
                            value, controller.userNewPasswordController.text);
                      },
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                          icon: Icon(
                            // Based on passwordVisible state choose the icon
                            controller.reapeatpasswordVisible.value
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: controller.reapeatpasswordVisible.value
                                ? AppColors.primaryColor
                                : AppColors.secondaryColor,
                          ),
                          onPressed: () {
                            // Update the state i.e. toogle the state of passwordVisible variable
                            controller.reapeatpasswordVisible.value =
                                !controller.reapeatpasswordVisible.value;
                          },
                        ),
                        hintText: 'Repeat New password'.tr,
                        hintStyle: TextStyle(
                          fontSize: Dimens.fontSize12,
                          color: Colors.grey,
                        ),
                      ),
                    )),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                  child: Container(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          onSurface: AppColors.secondaryColor),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          controller.changePass();
                        } else {
                          autovalidate = AutovalidateMode.onUserInteraction;
                        }
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                          colors: [AppColors.secondaryColor, Colors.redAccent],
                        )),
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          "Save".tr,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 200,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
