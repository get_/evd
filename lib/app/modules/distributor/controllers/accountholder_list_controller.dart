import 'package:evd_mobile/app/modules/distributor/distributor_model.dart';
import 'package:evd_mobile/app/modules/distributor/views/profile_page.dart';
import 'package:evd_mobile/app/modules/printpage/api_error_model.dart';
import 'package:evd_mobile/app/modules/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:evd_mobile/app/common/storage/storage.dart';
import 'package:evd_mobile/app/data/api_helper.dart';
import 'package:logger/logger.dart';

class AccountholderController extends GetxController
    with StateMixin<Distributer> {
  final ApiHelper _apiHelper = Get.find();
  TextEditingController userOldPasswordController = TextEditingController();
  TextEditingController userNewPasswordController = TextEditingController();
  TextEditingController userRepeatPasswordController = TextEditingController();
  RxBool currentpasswordVisible = true.obs;
  RxBool newpasswordVisible = true.obs;
  RxBool reapeatpasswordVisible = true.obs;

  Distributer ds = Distributer(
      fullName: '',
      telephone: '0909090909',
      commission: 0.0,
      balance: 0.0,
      accountType: 00,
      isActive: true,
      parentId: '',
      loginId: '',
      allowChild: true,
      regDate: 0,
      address: '',
      companyName: '',
      email: '',
      postCode: '',
      username: '',
      agentUsername: '');

  @override
  void onReady() {
    super.onReady();
    if (Storage.storage.hasData('token')) {
      getAccountHolder();
    } else {}
  }

  @override
  void onInit() {
    super.onInit();
  }

  void clear() {
    userNewPasswordController.clear();
    userOldPasswordController.clear();
    userRepeatPasswordController.clear();
  }

  void getAccountHolder() {
    change(null, status: RxStatus.empty());
    _apiHelper.getDistributorDetail().futureValue(
      (dynamic value) {
        Logger().d('dddddddddddddddd' + value.toString());
        ds = DistributerFromJson(value);
        change(DistributerFromJson(value), status: RxStatus.success());
        Logger().d(value);
      },
      retryFunction: getAccountHolder,
    );
  }

  void changePass() {
    _apiHelper
        .changePassword(userOldPasswordController.text,
            userNewPasswordController.text, userRepeatPasswordController.text)
        .then((value) {
      if (value.statusCode == 204) {
        toast('Password changed Successfully'.tr);
        printInfo(info: value.body.toString());
        clear();
        Get.off(() => profilepage());
      } else {
        if (value.body == null) {
          toast('Something went wrong'.tr);
        } else {
          Logger().d(value.body);
          var err = apiErrorFromJson(value.body);
          toast(err.msgs[0]);
        }
      }
    });
  }

  void onEditProfileClick() {
    Get.back();
  }

  void onFaqsClick() {
    Get.back();
  }

  void onLogoutClick() {
    Storage.clearStorage();
    // Get.offAllNamed(Routes.HOME);
    //Specify the INITIAL SCREEN you want to display to the user after logout
  }
}
