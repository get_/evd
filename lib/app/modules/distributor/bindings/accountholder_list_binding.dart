import 'package:evd_mobile/app/modules/distributor/controllers/accountholder_list_controller.dart';
import 'package:get/get.dart';

class AccountholderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AccountholderController>(
      () => AccountholderController(),
    );
  }
}
