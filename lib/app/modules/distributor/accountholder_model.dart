// To parse this JSON data, do
//
//     final AccountHolder = AccountHolderFromJson(jsonString);

import 'dart:convert';

AccountHolder AccountHolderFromJson(String str) =>
    AccountHolder.fromJson(json.decode(str));

String AccountHolderToJson(AccountHolder data) => json.encode(data.toJson());

class AccountHolder {
  AccountHolder({
    required this.data,
    this.total,
  });

  List<Datum> data;
  int? total;

  factory AccountHolder.fromJson(Map<String, dynamic> json) => AccountHolder(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class Datum {
  Datum({
    this.id,
    required this.fullname,
    required this.telephone,
    required this.commission,
    required this.balance,
    required this.accountType,
    required this.isActive,
    this.parentId,
    required this.allowChild,
    this.loginId,
    required this.regDate,
  });

  String? id;
  String fullname;
  String telephone;
  double commission;
  double balance;
  int accountType;
  bool isActive;
  String? parentId;
  bool allowChild;
  String? loginId;
  double regDate;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        fullname: json["full_name"],
        telephone: json["telephone"],
        commission: json["commission"],
        balance: json["balance"],
        accountType: json["account_type"],
        isActive: json["is_active"],
        parentId: json["parent_id"],
        allowChild: json["allow_child"],
        loginId: json["login_id"],
        regDate: json["reg_date"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "full_name": fullname,
        "telephone": telephone,
        "commission": commission,
        "balance": balance,
        "account_type": accountType,
        "is_active": isActive,
        "parent_id": parentId,
        "allow_child": allowChild,
        "login_id": loginId,
        "reg_date": regDate,
      };
}
