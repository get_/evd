// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

Distributer DistributerFromJson(String str) =>
    Distributer.fromJson(json.decode(str));

String DistributerToJson(Distributer data) => json.encode(data.toJson());

class Distributer {
  Distributer({
    this.id,
    required this.fullName,
    required this.telephone,
    required this.commission,
    required this.balance,
    required this.accountType,
    required this.isActive,
    required this.parentId,
    required this.loginId,
    required this.allowChild,
    required this.regDate,
    required this.username,
    required this.email,
    required this.address,
    required this.companyName,
    required this.postCode,
    required this.agentUsername,
  });

  String? id;
  String fullName;
  String telephone;
  double commission;
  double balance;
  int accountType;
  bool isActive;
  String parentId;
  String loginId;
  bool allowChild;
  int regDate;
  String username;
  String agentUsername;
  String email;
  String address;
  String companyName;
  String postCode;



  factory Distributer.fromJson(Map<String, dynamic> json) => Distributer(
        id: json["id"],
        fullName: json["full_name"],
        telephone: json["telephone"],
        commission: json["commission"],
        balance: json["balance"],
        accountType: json["account_type"],
        isActive: json["is_active"],
        parentId: json["parent_id"],
        loginId: json["login_id"],
        allowChild: json["allow_child"],
        regDate: json["reg_date"],
        username: json["username"],
        email: json["email"],
        address: json["address"],
        companyName: json["company_name"],
        postCode: json["post_code"],
        agentUsername:json["agent_username"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "full_name": fullName,
        "telephone": telephone,
        "commission": commission,
        "balance": balance,
        "account_type": accountType,
        "is_active": isActive,
        "parent_id": parentId,
        "login_id": loginId,
        "allow_child": allowChild,
        "reg_date": regDate,
        "username": username,
        "email": email,
        "address": address,
        "company_name": companyName,
        "post_code": postCode,
        "agent_username":agentUsername
      };
}
