import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:evd_mobile/app/common/util/utils.dart';
import 'package:evd_mobile/app/modules/bulkdetail/select_print_model.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

class SelectReprint {
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;

  SelectPrintVoucher? vouchure;
  SelectReprint({this.vouchure, required this.agent});

  String agent;

  getDate() {
    return Utils.toDate(vouchure!.regDate);
  }

  sample(String pathImage) async {
    var s = vouchure!.voucherNumber.split("");
    var first = s.take(5).toList();
    print(first.join(""));

    var seco = vouchure!.voucherNumber
        .substring(5, vouchure!.voucherNumber.length - 5);
    print(seco);
    var thr = vouchure!.voucherNumber.substring(
        vouchure!.voucherNumber.length - 5, vouchure!.voucherNumber.length);
    print(thr);
    var tot = first.join("").toString() +
        " " +
        seco.toString() +
        " " +
        thr.toString();

    bluetooth.isConnected.then((isConnected) async {
      if (isConnected!) {
        var now = DateTime.now();
        var formatter = DateFormat('dd-MM-yyyy');
        String formattedDate = formatter.format(now);
        Logger().i(formattedDate);
        bluetooth.printImage(pathImage);
        bluetooth.printCustom("${vouchure!.faceValue} Birr", 1, 1);
        bluetooth.printCustom(
            vouchure!.voucherNumber.isEmpty ? vouchure!.voucherNumber : tot,
            3,
            1);
        bluetooth.printLeftRight('', 'Duplicated', 0);
        bluetooth.printLeftRight("serial ", vouchure!.serialNo, 0);
        bluetooth.printLeftRight("Print date ", formattedDate, 0);
        bluetooth.printLeftRight("Agent ", agent, 0);
        bluetooth.printLeftRight("To recharge dail", "*805*PIN#", 0);
        bluetooth.printCustom("----------------------------", 1, 1);
        // bluetooth.printNewLine();
        // bluetooth.paperCut();
      }
    });
  }

}
