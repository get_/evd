// ignore_for_file: invalid_use_of_protected_member

import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widgets/base_widget.dart';
import '../controllers/bluetoothconnection_controller.dart';

class BluetoothconnectionView extends GetView<BluetoothconnectionController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Connect Printer'),
        centerTitle: true,
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Device:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Obx(() => controller.devices.isEmpty
                      ? Center(child: Text('no Device'.tr))
                      : DropdownButton<BluetoothDevice>(
                          focusColor: Colors.white,
                          value: controller.device.value.name == ''
                              ? controller.devices[0]
                              : controller.device.value,
                          style: TextStyle(color: Colors.white),
                          iconEnabledColor: Colors.black,
                          items: controller.devices
                              .map<DropdownMenuItem<BluetoothDevice>>(
                                  (product) {
                            return DropdownMenuItem<BluetoothDevice>(
                              value: product,
                              child: Text(
                                product.name!,
                                style: TextStyle(color: Colors.orange),
                              ),
                            );
                          }).toList(),
                          onChanged: (value) {
                            controller.device.value = value!;
                          },
                        )),
                  // Expanded(
                  //     child: Obx(
                  //   () => DropdownButton(
                  //     items: _getDeviceItems(),
                  //     onChanged: (value) => controller.device.value =
                  //         (value as BluetoothDevice?)!,
                  //     value: controller.device,
                  //   ),
                  // )),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: AppColors.primaryColor),
                    onPressed: () {
                      print('refresh');
                      controller.initPlatformState();
                    },
                    child: Text(
                      'Refresh',
                      style: TextStyle(color: AppColors.whiteColor),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Obx(() => ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: controller.connected.value
                                ? Colors.red
                                : Colors.green),
                        onPressed: controller.connected.value
                            ? controller.disconnect
                            : controller.connect,
                        child: controller.connecting.value
                            ? CircularProgressIndicator(
                                color: AppColors.secondaryColor,
                              )
                            : Text(
                                controller.connected.value
                                    ? 'Disconnect'
                                    : 'Connect',
                                style: TextStyle(color: Colors.white),
                              ),
                      )),
                ],
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 10.0, right: 10.0, top: 50),
                child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(primary: AppColors.primaryColor),
                  onPressed: () async {
                    print('print started.....');
                    controller.testPrint!.sample(controller.pathImage.value);
                  },
                  child: Text('START PRINT ',
                      style: TextStyle(color: Colors.white)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}
