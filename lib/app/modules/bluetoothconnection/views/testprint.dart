import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

import '../../../common/util/utils.dart';
import '../../printpage/vochure_model.dart';
import '../../voucherhistory/reprint_model.dart';

class TestPrint {
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
  Datum? vouchure;
  String agent;
  PrintModel? reprint;
  TestPrint({this.vouchure, this.reprint, required this.agent});
  getDate() {
    return Utils.toDate(vouchure!.regDate);
  }

  sample(String pathImage) async {
    //SIZE
    // 0- normal size text
    // 1- only bold text
    // 2- bold with medium text
    // 3- bold with large text
    //ALIGN
    // 0- ESC_ALIGN_LEFT
    // 1- ESC_ALIGN_CENTER
    // 2- ESC_ALIGN_RIGHT

    var s = vouchure!.voucherNumber.split("");
    var first = s.take(5).toList();
    print(first.join(""));

    var seco = vouchure!.voucherNumber
        .substring(5, vouchure!.voucherNumber.length - 5);
    print(seco);
    var thr = vouchure!.voucherNumber.substring(
        vouchure!.voucherNumber.length - 5, vouchure!.voucherNumber.length);
    print(thr);
    var tot = first.join("").toString() +
        " " +
        seco.toString() +
        " " +
        thr.toString();

    bluetooth.isConnected.then(
      (isConnected) {
        var now = DateTime.now();
        var formatter = DateFormat('dd-MM-yyyy');
        String formattedDate = formatter.format(now);
        Logger().i(formattedDate);
        print(formattedDate); // 2016-01-25
        if (isConnected!) {
          bluetooth.printImage(pathImage); //path of your image/logo
          bluetooth.printCustom("${vouchure!.faceValue} Birr", 1, 1);
          bluetooth.printCustom(tot, 3, 1);
          bluetooth.printLeftRight("serial ", vouchure!.serialNo, 0);
          bluetooth.printLeftRight("Print date ", formattedDate, 0);
          bluetooth.printLeftRight("Agent ", agent, 0);
          bluetooth.printLeftRight("To recharge dail", "*805*PIN#", 0);
          bluetooth.printCustom("-------------------------------", 1, 1);
          // bluetooth.printNewLine();
          // bluetooth.paperCut();
        }
      },
    );
  }
}
