// ignore_for_file: unused_import

import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

import '../../../common/util/exports.dart';
import '../../bulkdetail/single_reprint_responce_model.dart';

class SinglePrintTest {
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
  SingleReprintResponce? singleReprint;
  SinglePrintTest(this.singleReprint, this.agent);
  String agent;
  sample(String pathImage) async {
    //SIZE
    // 0- normal size text
    // 1- only bold text
    // 2- bold with medium text
    // 3- bold with large text
    //ALIGN
    // 0- ESC_ALIGN_LEFT
    // 1- ESC_ALIGN_CENTER
    // 2- ESC_ALIGN_RIGHT

    var s = singleReprint!.voucherNumber.split("");
    var first = s.take(5).toList();
    print(first.join(""));

    var seco = singleReprint!.voucherNumber
        .substring(5, singleReprint!.voucherNumber.length - 5);
    print(seco);
    var thr = singleReprint!.voucherNumber.substring(
        singleReprint!.voucherNumber.length - 5,
        singleReprint!.voucherNumber.length);
    print(thr);
    var tot = first.join("").toString() +
        " " +
        seco.toString() +
        " " +
        thr.toString();

    bluetooth.isConnected.then((isConnected) {
      if (isConnected!) {
        var now = DateTime.now();
        var formatter = DateFormat('dd-MM-yyyy');
        String formattedDate = formatter.format(now);
        Logger().i(formattedDate);
        bluetooth.printImage(pathImage); //path of your image/logo
        bluetooth.printCustom("${singleReprint!.faceValue} Birr", 1, 1);
        bluetooth.printCustom(tot, 3, 1);
        bluetooth.printLeftRight('', 'Duplicated', 0);
        bluetooth.printLeftRight("serial ", singleReprint!.serialNo, 0);
        bluetooth.printLeftRight("Print date ", formattedDate, 0);
        bluetooth.printLeftRight("Agent ", agent, 0);
        bluetooth.printLeftRight("To recharge dail", "*805*PIN#", 0);
        bluetooth.printCustom("-------------------------------", 1, 1);
        // bluetooth.printNewLine();
        // bluetooth.paperCut();
      }
    });
  }
}
