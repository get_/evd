import 'dart:io';

import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:evd_mobile/app/modules/bluetoothconnection/views/select_print.dart';
import 'package:evd_mobile/app/modules/bulkdetail/select_print_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';

import '../../../common/util/exports.dart';
import '../../bulkdetail/single_reprint_responce_model.dart';
import '../../printpage/vochure_model.dart';
import '../../voucherhistory/bulk_reprint_model.dart';
import '../../widgets/toast.dart';
import '../views/bulk_print.dart';
import '../views/single_print.dart';
import '../views/testprint.dart';

class BluetoothconnectionController extends GetxController {
  final count = 0.obs;

  RxBool enableBle = false.obs;

  bool toggle() {
    enableBle.value = !enableBle.value;

    return connected.value;
  }

  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;

  RxList<BluetoothDevice> devices = <BluetoothDevice>[].obs;
  Rx<BluetoothDevice> device = BluetoothDevice('', '').obs;

  RxBool connected = false.obs;
  RxString pathImage = ''.obs;
  BulkPrintTest? testReprint;
  SelectReprint? selectReprint;
  SinglePrintTest? singlePrint;
  TestPrint? testPrint;
  RxBool connecting = false.obs;

  initSavetoPath() async {
    final filename = 'yourlogo.png';
    var bytes = await rootBundle.load("assets/images/teleee.png");
    String dir = (await getApplicationDocumentsDirectory()).path;
    writeToFile(bytes, '$dir/$filename');

    pathImage.value = '$dir/$filename';
  }

  getConnectedDevice() {
    return device.value;
  }

  Future<void> initPlatformState() async {
    try {
      bool? isConnected = await bluetooth.isConnected;
      bool? isOn = await bluetooth.isOn;
      if (!isOn!) {
        toast('Turn on blutooth.'.tr);
      } else {
        List<BluetoothDevice> devicess = [];
        try {
          devicess = await bluetooth.getBondedDevices();
          devices.value = devicess;
        } on PlatformException catch (err) {
          Logger().i(err);
          toast('Please give access to location.'.tr);
        } catch (er) {
          Logger().i(er);
        }
        bluetooth.onStateChanged().listen((state) {
          switch (state) {
            case BlueThermalPrinter.CONNECTED:
              connected.value = true;
              break;
            case BlueThermalPrinter.DISCONNECTED:
              connected.value = false;
              break;
            default:
              print(state);
              break;
          }
        });

        if (isConnected!) {
          connected.value = true;
        }
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> disconnect() async {
    try {
      connecting.value = true;
      bluetooth.disconnect();

      bluetooth.isConnected.then((value) {
        connected.value = value!;
        connecting.value = false;
      });
    } catch (e) {
      rethrow;
    }
  }

//write to app path
  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  Future<void> connect() async {
    try {
      bool? isOn = await bluetooth.isOn;
      if (!isOn!) {
        toast('Turn on blutooth.'.tr);
      } else {
        if (device.value.name == '') {
          toast('No device selected.'.tr);
        } else {
          try {
            connecting.value = true;
            bluetooth.isConnected.then((isConnected) async {
              if (!isConnected!) {
                bluetooth.connect(device.value).then((value) async {
                  connecting.value = false;
                  if (value == true) {
                    toast('device connected sucessfully.'.tr);
                    connected.value = (await bluetooth.isConnected)!;
                    connecting.value = false;
                  } else {
                    toast('The selected device is not available.'.tr);
                    connected.value = (await bluetooth.isConnected)!;
                    connecting.value = false;
                  }
                }).catchError((error) async {
                  toast('The selected device is not available.'.tr);
                  connected.value = (await bluetooth.isConnected)!;
                  connecting.value = false;
                });

                connected.value = (await bluetooth.isConnected)!;
              } else {
                toast('check if the selected device is available.'.tr);
                connecting.value = false;
              }
            });
          } catch (e) {
            Logger().d(e);
          }
        }
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<bool?> checkDeviceBluetoothIsOn() async {
    return await bluetooth.isOn;
  }

  openBluSetting() {
    bluetooth.openSettings;
  }

  Future show(
    String message, {
    Duration duration: const Duration(seconds: 3),
  }) async {
    await Future.delayed(Duration(milliseconds: 100));
    ScaffoldMessenger.of(Get.context!).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        duration: duration,
      ),
    );
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (Storage.storage.hasData('token')) {
      initPlatformState();
    }
    initSavetoPath();
  }

  @override
  void onClose() {}
  startPrint(List<Datum> values, String agent) async {
    try {
      bool? isOn = await bluetooth.isOn;
      if (!isOn!) {
        show('Turn No Bluetooth First'.tr);
      } else {
        for (var element in values) {
          testPrint = TestPrint(vouchure: element, agent: agent);
          testPrint!.sample(pathImage.value);
        }
      }
    } catch (e) {
      rethrow;
    }
  }

  startRePrint(List<BulkReprint> values, String agent) async {
    bool? isOn = await bluetooth.isOn;
    if (!isOn!) {
      show('Turn No Bluetooth First'.tr);
    } else {
      bool? isConnected = await bluetooth.isConnected;
      if (!isConnected!) {
        show('No Bluetooth Connection'.tr);
      } else {
        for (var element in values) {
          testReprint = BulkPrintTest(vouchure: element, agent: agent);
          testReprint!.sample(pathImage.value);
        }
      }
    }
  }

  startSingleRePrint(SingleReprintResponce value, String agent) async {
    bool? isOn = await bluetooth.isOn;
    if (!isOn!) {
      show('Turn No Bluetooth First'.tr);
    } else {
      bool? isConnected = await bluetooth.isConnected;
      if (!isConnected!) {
        show('No Bluetooth Connection'.tr);
      } else {
        singlePrint = SinglePrintTest(value, agent);
        singlePrint!.sample(pathImage.value);
      }
    }
  }

  startSelectRePrint(List<SelectPrintVoucher> values, String agent) async {
    bool? isOn = await bluetooth.isOn;
    if (!isOn!) {
      show('Turn No Bluetooth First'.tr);
    } else {
      bool? isConnected = await bluetooth.isConnected;
      if (!isConnected!) {
        show('No Bluetooth Connection'.tr);
      } else {
        for (var element in values) {
          selectReprint = SelectReprint(vouchure: element, agent: agent);
          selectReprint!.sample(pathImage.value);
        }
      }
    }
  }
}
