import 'package:get/get.dart';

import '../controllers/bluetoothconnection_controller.dart';

class BluetoothconnectionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BluetoothconnectionController>(
      () => BluetoothconnectionController(),
    );
  }
}
