import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../data/api_helper.dart';
import '../../distributor/controllers/accountholder_list_controller.dart';
import '../../summaryreport/controllers/summaryreport_controller.dart';
import '../../summaryreport/summaryreport_model.dart';

class DailysalesController extends GetxController {
  final count = 0.obs;
  @override
  final ApiHelper _apiHelper = Get.find();
  AccountholderController account = Get.put(AccountholderController());

  var title = Get.arguments['title'];
  List<Daily> data = Get.arguments['data'];
  var reportContr = Get.put(SummaryreportController());
  Map<int, Daily> soldCards = {};
  double sum = 0.0;
  double sum_birr = 0.0;
  double total_sale = 0.0;
  double total_profit = 0.0;
  @override
  void onInit() {
    super.onInit();
    for (var element in data) {
      sum += element.numberOfCards;
      soldCards[element.faceValue] = element;
      sum_birr += element.balance;
      total_sale += element.numberOfCards * element.faceValue;
    }
    total_sale = total_sale - (total_sale * (account.ds.commission / 100));
    total_profit += (sum_birr * (account.ds.commission / 100));
  }

  @override
  void onReady() {
    Logger().d(data.length);
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
