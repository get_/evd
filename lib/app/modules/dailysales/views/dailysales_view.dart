import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/constants.dart';
import '../../home/controllers/home_controller.dart';
import '../../widgets/base_widget.dart';
import '../controllers/dailysales_controller.dart';

class DailysalesView extends GetView<DailysalesController> {
  final homeController = Get.find<HomeController>();
  var controller = Get.put(DailysalesController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Align(
            alignment: Alignment.centerLeft, child: Text(controller.title)),
        backgroundColor: AppColors.primaryColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 35),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    ' Cards'.tr,
                    style: TextStyle(
                        fontSize: Dimens.fontSize16,
                        fontWeight: FontWeight.bold),
                  ),
                  Text('Quantity'.tr,
                      style: TextStyle(
                          fontSize: Dimens.fontSize16,
                          fontWeight: FontWeight.bold))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                  height: MediaQuery.of(context).size.height * 0.55,
                  child: ListView.builder(
                      itemCount: Constants.FACEVALUE.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          leading: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RichText(
                                text: TextSpan(
                                  text: Constants.FACEVALUE[index].toString(),
                                  style: TextStyle(
                                    color: AppColors.secondaryColor,
                                    fontSize: Dimens.fontSize18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: ' BIRR'.tr,
                                        style: TextStyle(
                                          color: Colors.orangeAccent,
                                          fontSize: Dimens.fontSize14,
                                        )),
                                  ],
                                ),
                              ),
                              Text(
                                  controller
                                              .soldCards[
                                                  Constants.FACEVALUE[index]]
                                              ?.balance
                                              .toString() ==
                                          null
                                      ? '0.00'
                                      : '${Utils.numberFormat(controller.soldCards[Constants.FACEVALUE[index]]?.balance)}',
                                  style: TextStyle(
                                      color: AppColors.primaryColor,
                                      fontSize: Dimens.fontSize16,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          trailing: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  controller
                                              .soldCards[
                                                  Constants.FACEVALUE[index]]
                                              ?.numberOfCards
                                              .toInt() ==
                                          null
                                      ? '0'
                                      : '${controller.soldCards[Constants.FACEVALUE[index]]?.numberOfCards.toInt().toString()}',
                                  style: TextStyle(
                                    color: AppColors.primaryColor,
                                    fontSize: Dimens.fontSize18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            ],
                          ),
                        );
                      })),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: Colors.orange.shade100,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Total'.tr,
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontWeight: FontWeight.bold,
                                fontSize: Dimens.fontSize20),
                          ),
                          Text(
                            ' ${Utils.numberFormat(controller.sum_birr)}',
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontSize: Dimens.fontSize20,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Commission'.tr,
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontWeight: FontWeight.bold,
                                fontSize: Dimens.fontSize20),
                          ),
                          Text(
                            '${controller.account.ds.commission}\%',
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontSize: Dimens.fontSize20,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Net'.tr,
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontWeight: FontWeight.bold,
                                fontSize: Dimens.fontSize20),
                          ),
                          Text(
                            '${Utils.numberFormat(controller.total_sale)}',
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontSize: Dimens.fontSize20,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
