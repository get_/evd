import 'package:get/get.dart';

import '../controllers/dailysales_controller.dart';

class DailysalesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DailysalesController>(
      () => DailysalesController(),
    );
  }
}
