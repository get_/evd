// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const LOGIN = _Paths.LOGIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const ONBOARDING = _Paths.ONBOARDING;
  static const ADD_ACCOUNTHOLDER = _Paths.ADD_ACCOUNTHOLDER;
  static const EDIT_ACCOUNTHOLDER = _Paths.EDIT_ACCOUNTHOLDER;
  static const LIST_ACCOUNTHOLDER = _Paths.LIST_ACCOUNTHOLDER;
  static const BATCH = _Paths.BATCH;
  static const DAILYSALES = _Paths.DAILYSALES;
  static const SUMMARYREPORT = _Paths.SUMMARYREPORT;
  static const DEPOSIT = _Paths.DEPOSIT;
  static const PRINTPAGE = _Paths.PRINTPAGE;
  static const PRINTPREVIEW = _Paths.PRINTPREVIEW;
  static const BLUETOOTHCONNECTION = _Paths.BLUETOOTHCONNECTION;
  static const VOUCHERHISTORY = _Paths.VOUCHERHISTORY;
  static const BULKDETAIL = _Paths.BULKDETAIL;
  static const PRINTHISTORY = _Paths.PRINTHISTORY;
  static const SPLASH_SCREEN = _Paths.SPLASH_SCREEN;
}

abstract class _Paths {
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
  static const ONBOARDING = '/onboarding';
  static const LIST_ACCOUNTHOLDER = '/list-distributor';
  static const ADD_ACCOUNTHOLDER = '/add-distributor';
  static const EDIT_ACCOUNTHOLDER = '/edit-distributor';
  static const BATCH = '/batch';
  static const DAILYSALES = '/dailysales';
  static const SUMMARYREPORT = '/summaryreport';
  static const DEPOSIT = '/deposit';
  static const PRINTPAGE = '/printpage';
  static const PRINTPREVIEW = '/printpreview';
  static const BLUETOOTHCONNECTION = '/bluetoothconnection';
  static const VOUCHERHISTORY = '/voucherhistory';
  static const BULKDETAIL = '/bulkdetail';
  static const PRINTHISTORY = '/printhistory';
  static const SPLASH_SCREEN = '/splash-screen';
}
