import 'package:evd_mobile/app/modules/distributor/bindings/accountholder_list_binding.dart';
import 'package:evd_mobile/app/modules/distributor/views/profile_page.dart';
import 'package:get/get.dart';

import '../modules/batch/bindings/batch_binding.dart';
import '../modules/batch/views/batch_view.dart';
import '../modules/bluetoothconnection/bindings/bluetoothconnection_binding.dart';
import '../modules/bluetoothconnection/views/bluetoothconnection_view.dart';
import '../modules/bulkdetail/bindings/bulkdetail_binding.dart';
import '../modules/bulkdetail/views/bulkdetail_view2.dart';
import '../modules/dailysales/bindings/dailysales_binding.dart';
import '../modules/dailysales/views/dailysales_view.dart';
import '../modules/deposit/bindings/deposit_binding.dart';
import '../modules/deposit/views/deposit_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/printhistory/bindings/printhistory_binding.dart';
import '../modules/printhistory/views/printhistory_view.dart';
import '../modules/printpage/bindings/printpage_binding.dart';
import '../modules/printpage/views/printpage_view.dart';
import '../modules/summaryreport/bindings/summaryreport_binding.dart';
import '../modules/summaryreport/views/summaryreport_view.dart';
import '../modules/voucherhistory/bindings/voucherhistory_binding.dart';
import '../modules/voucherhistory/views/voucherhistory_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.LIST_ACCOUNTHOLDER,
      page: () => profilepage(),
      binding: AccountholderBinding(),
    ),
    GetPage(
      name: _Paths.BATCH,
      page: () => BatchView(),
      binding: BatchBinding(),
    ),
    GetPage(
      name: _Paths.DAILYSALES,
      page: () => DailysalesView(),
      binding: DailysalesBinding(),
    ),
    GetPage(
      name: _Paths.SUMMARYREPORT,
      page: () => SummaryreportView(),
      binding: SummaryreportBinding(),
    ),
    GetPage(
      name: _Paths.DEPOSIT,
      page: () => DepositView(),
      binding: DepositBinding(),
    ),
    GetPage(
      name: _Paths.PRINTPAGE,
      page: () => PrintpageView(),
      binding: PrintpageBinding(),
    ),
    GetPage(
      name: _Paths.BLUETOOTHCONNECTION,
      page: () => BluetoothconnectionView(),
      binding: BluetoothconnectionBinding(),
    ),
    GetPage(
      name: _Paths.VOUCHERHISTORY,
      page: () => VoucherhistoryView(),
      binding: VoucherhistoryBinding(),
    ),
    GetPage(
      name: _Paths.BULKDETAIL,
      page: () => BulkdetailView(),
      binding: BulkdetailBinding(),
    ),
    GetPage(
      name: _Paths.PRINTHISTORY,
      page: () => PrinthistoryView(),
      binding: PrinthistoryBinding(),
    ),
  ];
}
