import 'package:get/get.dart';

import '../modules/login/user_login_model.dart';
import '../modules/printpage/print_data_model.dart';

export 'package:evd_mobile/app/common/util/extensions.dart';
export 'package:evd_mobile/app/common/util/utils.dart';

abstract class ApiHelper {
  Future<Response> getDistributorDetail();
  Future<Response> getDeposit();
  Future<Response> getVoucheWithFaceValue(int value);
  Future<Response> login(DistributerModel distributerModel);
  Future<Response> fetchSummaryReport();
  Future<Response> printRequest(Welcome body);
  Future<Response> getAccountBalance();
  Future<Response> getAllVoucher();
  Future<Response> bulkReprint(String id);
  // Future<Response> getPrintedVouchers();
  Future<Response> changePassword(String old, String newp, String repeat);

  Future<Response> getPrintedVouchers(page);
  Future<Response> getBulkPrintVouchers(id, page);
  Future<Response> singleReprint(id);
  Future<Response> getActiveVoucherNumber();
  Future<Response> selectReprint(data);
}
