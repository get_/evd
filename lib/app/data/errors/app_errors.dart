import 'package:get/get.dart';
import 'package:evd_mobile/app/common/util/exports.dart';

abstract class AppErrors implements Exception {
  String? get message;

  AppErrors() {
    printInfo(info: message!);
  }
}

class ApiError extends AppErrors {
  @override
  final String message;

  ApiError({
    this.message = Strings.unknownError,
  });
}

class TimeoutError extends AppErrors {
  @override
  final String? message;

  TimeoutError({
    this.message = Strings.connectionTimeout,
  });
}

class NoConnectionError extends AppErrors {
  @override
  final String message;

  NoConnectionError({
    this.message = Strings.noConnection,
  });
}
class ServerError extends AppErrors {
  @override
  final String message;

  ServerError({
    this.message = Strings.serverErroe,
  });
}

class UnauthorizeError extends AppErrors {
  @override
  final String message;

  UnauthorizeError({
    this.message = Strings.unauthorize,
  });
}
