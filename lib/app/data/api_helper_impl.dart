import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../common/constants.dart';
import '../common/storage/storage.dart';
import '../modules/login/user_login_model.dart';
import '../modules/printpage/print_data_model.dart';
import 'api_helper.dart';

class ApiHelperImpl extends GetConnect with ApiHelper {
  @override
  void onInit() {
    httpClient.baseUrl = Constants.BASE_URL;
    httpClient.timeout = Constants.timeout;
    addRequestModifier();
    addResponseModifier();
  }

  void addResponseModifier() {
    httpClient.addResponseModifier((request, response) {
      Logger().d(
          'status code----->: ${response.statusCode}\nDATA----->: ${response.body} ');
      printInfo(
        info: 'Status Code: ${response.statusCode}\n'
            'Data: ${response.bodyString?.toString() ?? ''}',
      );

      return response;
    });
  }

  void addRequestModifier() {
    httpClient.addRequestModifier<dynamic>((request) {
      if (Storage.hasData(Constants.TOKEN!)) {
        Logger().d('eeeeeeeeee---->: ' + Storage.getValue(Constants.TOKEN!));
        request.headers['Authorization'] =
            'Bearer ' + Storage.getValue(Constants.TOKEN!);
      }
      String x = 'REQUEST ║ ${request.method.toUpperCase()}\n'.toString();
      var y = 'url: ${request.url}\n'.toString();
      var z = 'Headers: ${request.headers}\n'.toString();
      var xz = 'Body: ${request.files?.toString() ?? ''}\n'.toString();

      Logger().d(x + y + z + xz);

      printInfo(
        info: 'REQUEST ║ ${request.method.toUpperCase()}\n'
            'url: ${request.url}\n'
            'Headers: ${request.headers}\n'
            'Body: ${request.files?.toString() ?? ''}\n',
      );

      return request;
    });
  }

  @override
  Future<Response<dynamic>> getDistributorDetail() {
    return get('/distributor');
  }

  @override
  Future<Response> getVoucheWithFaceValue(int value) {
    return get('/vouchers/faceValue/?st=$value');
  }

  @override
  Future<Response> getDeposit() {
    return get('/distributor/deposit');
  }

  @override
  Future<Response> login(DistributerModel distributerModel) {
    Logger().d(distributerModel.toJson());
    return post('/login', distributerModel.toJson());
  }

  @override
  Future<Response> fetchSummaryReport() {
    return get('/distributor/timelyReports');
  }

  @override
  Future<Response> printRequest(Welcome body) {
    printInfo(info: body.toString());
    Logger().d(body.toJson());
    return post('/distributor/print', body.toJson());
  }

  @override
  Future<Response> getAccountBalance() {
    return get('/distributor/accountbalance');
  }

  @override
  Future<Response> bulkReprint(String id) {
    return post('/distributor/bulkReprint/$id', {});
  }

  @override
  Future<Response> getPrintedVouchers(page) {
    return get('/distributor/print?st=&ps=10&pi=$page');
  }

  @override
  Future<Response> getAllVoucher() {
    return get('/allvouchers');
  }

  @override
  Future<Response> getBulkPrintVouchers(id, page) {
    return get('/printvouchers/print/$id?st=&ps=10&pi=$page');
  }

  @override
  Future<Response> singleReprint(id) {
    return post('/distributor/singleReprint/$id', {});
  }

  @override
  Future<Response> getActiveVoucherNumber() {
    return get('/vouchersCount');
  }

  @override
  Future<Response> changePassword(String curr, String newp, String repeat) {
    return post('/me/do', {
      'method': 'pwd_change',
      'payload': {'pwd_current': curr, 'pwd_new': newp, 'pwd_repeat': repeat}
    });
  }

  @override
  Future<Response> selectReprint(data) {
    return post(
        '/distributor/selectReprint', {'method': 'reprint', 'payload': data});
  }
  /////########THIS IS YOUR'S #######///////////////

  // Future<Response> selectReprint(data) {
  //   Logger().d(data.toString());
  // // post('/distributor/selectReprint', data);
  //   return post('/distributor/selectReprint/do', {
  //     'method': 'reprint',
  //     'payload': {'ids':data}
  //   });
  // }
}
