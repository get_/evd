// ignore_for_file: constant_identifier_names, non_constant_identifier_names

class Constants {
  static const timeout = Duration(seconds: 30);
  static const String BASE_URL = 'https://d.et-erp.com/evd1_api';
  static const String? TOKEN = 'token';
  static const List<int> FACEVALUE = [5, 10, 15, 25, 50, 100, 500, 1000];
  static List<String> SUMMARIES = ['Daily', 'Weekly', 'Monthly'];
}
