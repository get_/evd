import 'package:evd_mobile/app/common/util/exports.dart';

class AppImages {
  ///add app images here

  AppImages._();

  static final String nowifi = 'nowifi'.image;
  static final String logo = 'logo'.image;
  static final String profile = 'profile'.image;
  static final String nodata = 'nodata'.image;
  static final String pass = 'password'.image;
  static final String logout = 'logout'.image;
}
