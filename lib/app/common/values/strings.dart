import 'package:get/get_utils/src/extensions/internacionalization.dart';

class Strings {
  const Strings._();

  static const String unknownError =
      'Unknow error! Please try again after some time.';
  static const String connectionTimeout =
      'Connection timeout. Please try again';
  static const String serverErroe =
      'Service Unavaliable, Please try after some time';
  static const String noConnection =
      'No connection. Please turn on your internet!';
  static const String unauthorize = 'Unauthorize. Please login again!';
  static var appName = 'EVD'.tr;
  static var ok = 'Ok'.tr;
  static const String error = 'Error';
  static var noInternet = 'No internet. Please try again later.'.tr;
  static var logOut = 'Log out';
  static var retry = 'Retry'.tr;
  static const String somethingWentWrong = 'Something went wrong.';
  static var home = 'Home'.tr;
  static var about = 'About'.tr;
  static var history = 'Summary Report'.tr;
  static var emailAddress = 'Email Address'.tr;
  static var mobileNumber = 'Mobile Number'.tr;
  static var emailOrMobile = '$emailAddress or $mobileNumber'.tr;
  static var cantBeEmpty = "can't be empty.".tr;
  static var fieldCantBeEmpty = 'Field $cantBeEmpty'.tr;
  static var numberCantBeEmpty = '$emailOrMobile $cantBeEmpty'.tr;
  static var emailCantBeEmpty = 'Email $cantBeEmpty'.tr;
  static var enterValid = 'Please enter a valid'.tr;
  static var enterValidNumber = '$enterValid $mobileNumber.'.tr;
  static var enterValidEmail = '$enterValid email.'.tr;
  static var password = 'Password'.tr;
  static var confirmPassword = 'Confirm $password'.tr;
  static var enterPassword = 'Enter $password'.tr;
  static var passwordCantBeEmpty = '$password $cantBeEmpty'.tr;
  static var mustbe8 = 'must be at least 8 characters long.'.tr;
  static var passwordValidation = '$password $mustbe8'.tr;
  static var confirmPasswordValidation =
      '$password and Confirm password should be same.'.tr;
  static var otpValidation = 'Invalid OTP'.tr;
  static var gallery = 'Gallery'.tr;
  static var camera = 'Camera'.tr;
  static var mobile = 'Mobile'.tr;
  static var from = 'From'.tr;
  static var to = 'To'.tr;
  static var success = 'Success'.tr;
  static const String checkboxValidation = 'Dummy Checkbox Validation Message';

  //////on drawer
  ///
  static var printHistory = 'Print History'.tr;
  static var deposit = 'Deposit'.tr;
}
