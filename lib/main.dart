import 'package:evd_mobile/app/modules/login/views/login_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:evd_mobile/app/common/util/initializer.dart';
import 'package:evd_mobile/app/routes/app_pages.dart';
import 'app/common/util/localization.dart';
import 'app/modules/widgets/base_widget.dart';

void main() {
  Initializer.instance.init(() {
    runApp(MyApp());
  });
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      BoxConstraints(
        maxWidth: Get.width,
        maxHeight: Get.height,
      ),
      designSize: Get.size,
    );

    return GetMaterialApp(
      title: Strings.appName,
      debugShowCheckedModeBanner: false,
      theme: AppTheme.theme,
      translations: LocaleString(),
      locale: Locale('en', 'US'),
      home: LoginView(),
      getPages: AppPages.routes,
      builder: (_, child) => BaseWidget(
        child: child ?? SizedBox.shrink(),
      ),
    );
  }
}
